<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ClienteProveedorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ClienteProveedorRepository extends EntityRepository {

    public function filtroBusqueda($unidad, $clienteProveedor, $buscador, $unidadActual) {
        $query = $this->createQueryBuilder('e');
        $query->where('e.estado = :estado')->setParameter(':estado', 'A');
        if (isset($unidadActual) && $unidadActual != '') {
            $query->andWhere('e.unidadNegocio = :unidadactual')
                    ->setParameter(':unidadactual', $unidadActual);
        }

        if (isset($unidad) && $unidad != '') {
            $query->andWhere('e.unidadNegocio = :unidad')
                    ->setParameter(':unidad', $unidad);
        }
        if (isset($clienteProveedor) && $clienteProveedor != '') {
            $query->andWhere('e.clienteProveedor = :clienteProveedor')
                    ->setParameter(':clienteProveedor', $clienteProveedor);
        }
        if (isset($buscador) && $buscador != '') {
            $query->andWhere('e.razonSocial like :buscador or e.mail like :buscador')
                    ->setParameter(':buscador', '%'.$buscador.'%');
        }

        $query->orderBy('e.razonSocial', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function filtro($razonSocial, $unidad = NULL) {
        if (isset($unidad)) {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.razonSocial LIKE :razon AND e.unidadNegocio = :unidad')->setParameter(':activo', 'A')->setParameter(':razon', '%' . $razonSocial . '%')->setParameter(':unidad', $unidad);
        } else {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.razonSocial LIKE :razon')->setParameter(':activo', 'A')->setParameter(':razon', '%' . $razonSocial . '%');
        }
        return $query->getResult();
    }

    public function getAllActivas($unidad = NULL) {
        if (isset($unidad)) {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.unidadNegocio = :unidad ORDER BY e.clienteProveedor,e.razonSocial ASC')->setParameter(':activo', 'A')->setParameter(':unidad', $unidad);
        } else {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo ORDER BY e.clienteProveedor,e.razonSocial ASC')->setParameter(':activo', 'A');
        }
        return $query->getResult();
    }

    public function getAllProv($unidad = NULL) {
        if (isset($unidad)) {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.clienteProveedor = 2 AND e.unidadNegocio = :unidad ORDER BY e.razonSocial ASC')->setParameter(':activo', 'A')->setParameter(':unidad', $unidad);
        } else {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.clienteProveedor = 2 ORDER BY e.razonSocial ASC')->setParameter(':activo', 'A');
        }
        return $query->getResult();
    }

    public function getAllCli($unidad = NULL) {
        if (isset($unidad)) {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.clienteProveedor = 1 AND e.unidadNegocio = :unidad ORDER BY e.razonSocial ASC')->setParameter(':activo', 'A')->setParameter(':unidad', $unidad);
        } else {
            $query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.clienteProveedor = 1 ORDER BY e.razonSocial ASC')->setParameter(':activo', 'A');
        }
        return $query->getResult();
    }

}
