<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\FacturaRepository")
 * @ORM\Table(name="factura")
 */
class Factura {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCC")
     * @ORM\JoinColumn(name="movimientocc_id", referencedColumnName="id", nullable=true)
     * */
    private $movimientocc;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio")
     * @ORM\JoinColumn(name="listaPrecio_id", referencedColumnName="id")
     */
    protected $listaPrecio;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float")
     */
    protected $descuento;

    /**
     * @ORM\Column(type="float")
     */
    protected $bonificacion;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="factura", cascade={"persist", "remove"} )
     */
    protected $productosFactura;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="float")
     */
    protected $oro;

    /**
     * @ORM\Column(type="float")
     */
    protected $plata;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosFactura = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Factura
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Factura
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Factura
     */
    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float 
     */
    public function getBonificacion() {
        return $this->bonificacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Factura
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set listaPrecio
     *
     * @param \JOYAS\JoyasBundle\Entity\ListaPrecio $listaPrecio
     * @return Factura
     */
    public function setListaPrecio(\JOYAS\JoyasBundle\Entity\ListaPrecio $listaPrecio = null) {
        $this->listaPrecio = $listaPrecio;

        return $this;
    }

    /**
     * Get listaPrecio
     *
     * @return \JOYAS\JoyasBundle\Entity\ListaPrecio 
     */
    public function getListaPrecio() {
        return $this->listaPrecio;
    }

    /**
     * Add productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     * @return Factura
     */
    public function addProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura[] = $productosFactura;

        return $this;
    }

    /**
     * Remove productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     */
    public function removeProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura->removeElement($productosFactura);
    }

    /**
     * Get productosFactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosFactura() {
        return $this->productosFactura;
    }

    /**
     * Set movimientocc
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoCC $movimientocc
     * @return Factura
     */
    public function setMovimientocc(\JOYAS\JoyasBundle\Entity\MovimientoCC $movimientocc = null) {
        $this->movimientocc = $movimientocc;

        return $this;
    }

    /**
     * Get movimientocc
     *
     * @return \JOYAS\JoyasBundle\Entity\MovimientoCC 
     */
    public function getMovimientocc() {
        return $this->movimientocc;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Factura
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set oro
     *
     * @param string $oro
     * @return Documento
     */
    public function setOro($oro) {
        $this->oro = $oro;

        return $this;
    }

    /**
     * Get oro
     *
     * @return string 
     */
    public function getOro() {
        return $this->oro;
    }

    /**
     * Set plata
     *
     * @param string $plata
     * @return Documento
     */
    public function setPlata($plata) {
        $this->plata = $plata;

        return $this;
    }

    /**
     * Get plata
     *
     * @return string 
     */
    public function getPlata() {
        return $this->plata;
    }

}
