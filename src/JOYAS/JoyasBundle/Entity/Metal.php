<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JOYAS\JoyasBundle\Entity\TipoCosto;
use JOYAS\JoyasBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\MetalRepository")
 * @ORM\Table(name="metal")
 */
class Metal {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $sinonimo;

    /**
     * @ORM\OneToMany(targetEntity="Categoriasubcategoria", mappedBy="metal")
     */
    protected $categoriasubcategorias;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->categoriasubcategorias = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ListaPrecio
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Metal
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add categoriasubcategorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias
     * @return Metal
     */
    public function addCategoriasubcategoria(\JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias) {
        $this->categoriasubcategorias[] = $categoriasubcategorias;

        return $this;
    }

    /**
     * Remove categoriasubcategorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias
     */
    public function removeCategoriasubcategoria(\JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias) {
        $this->categoriasubcategorias->removeElement($categoriasubcategorias);
    }

    /**
     * Get categoriasubcategorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategoriasubcategorias() {
        return $this->categoriasubcategorias;
    }

    /**
     * Set sinonimo
     *
     * @param string $sinonimo
     * @return Metal
     */
    public function setSinonimo($sinonimo) {
        $this->sinonimo = $sinonimo;

        return $this;
    }

    /**
     * Get sinonimo
     *
     * @return string 
     */
    public function getSinonimo() {
        return $this->sinonimo;
    }

}
