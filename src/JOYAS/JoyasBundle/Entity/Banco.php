<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\BancoRepository")
 * @ORM\Table(name="banco")
 * @UniqueEntity(
 * 		fields = {"descripcion"},
 * 			message = "Ya existe este banco."
 * 		) 
 */
class Banco {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=300,nullable=false)     
     * @Assert\Length(
     *      max = 300,     
     *      maxMessage = "La descripción no puede superar los 300 caracteres"
     * )     
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=1})
     */
    protected $activo = true;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="banco")
     */
    protected $cheques;
     /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="bancos")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

  

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->cheques = new ArrayCollection();
  
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Banco
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Banco
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Add cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     * @return Banco
     */
    public function addCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques)
    {
        $this->cheques[] = $cheques;
    
        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques)
    {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheques()
    {
        return $this->cheques;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Banco
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }
}