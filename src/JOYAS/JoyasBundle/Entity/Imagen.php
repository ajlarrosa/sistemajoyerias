<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ImagenRepository")
 * @ORM\Table(name="imagen") 
 */
class Imagen {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=255,nullable=false)    
     */
    protected $path;

    /**
     * @ORM\Column(type="blob",nullable=false)    
     */
    protected $foto;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    public function getFotoSrc() {
        if (!is_null($this->foto) && $this->foto != '') {
            return stream_get_contents($this->foto);
        } else {
            return '';
        }
    }

    public function getEstadoString() {
        if ($this->estado == 'A') {
            return 'Activo';
        } else {//estado = 'I'
            return 'Inactivo';
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Imagen
     */
    public function setFoto($foto) {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto() {
        return $this->foto;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Imagen
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }


    /**
     * Set path
     *
     * @param string $path
     * @return Imagen
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
}