<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ProductoConsignacionRepository")
 * @ORM\Table(name="productoconsignacion")
 */
class ProductoConsignacion{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", length=100)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float", length=100)
     */
    protected $precio;

	/**
	* @ORM\ManyToOne(targetEntity="Producto", inversedBy="productosConsignacion")
	* @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
	*/
    protected $producto;

	/**
	* @ORM\ManyToOne(targetEntity="Consignacion", inversedBy="productosConsignacion", cascade={"persist"})
	* @ORM\JoinColumn(name="consignacion_id", referencedColumnName="id")
	*/
    protected $consignacion;
	
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return 'consignacion';
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return ProductoConsignacion
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    
        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return ProductoConsignacion
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    
        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ProductoConsignacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $producto
     * @return ProductoConsignacion
     */
    public function setProducto(\JOYAS\JoyasBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;
    
        return $this;
    }

    /**
     * Get producto
     *
     * @return \JOYAS\JoyasBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set consignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\Consignacion $consignacion
     * @return ProductoConsignacion
     */
    public function setConsignacion(\JOYAS\JoyasBundle\Entity\Consignacion $consignacion = null)
    {
        $this->consignacion = $consignacion;
    
        return $this;
    }

    /**
     * Get consignacion
     *
     * @return \JOYAS\JoyasBundle\Entity\Consignacion 
     */
    public function getConsignacion()
    {
        return $this->consignacion;
    }
}