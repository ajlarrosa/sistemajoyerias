<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ChequeRepository")
 * @ORM\Table(name="cheque")
 */
class Cheque{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaemision;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechacobro;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nrocheque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firmantes;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $conciliacion;

	/**
	* @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="cheques")
	* @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
	*/
    protected $unidadNegocio;

	/**
	* @ORM\ManyToOne(targetEntity="Documento", inversedBy="cheques",cascade={"persist"})
	* @ORM\JoinColumn(name="documento_id", referencedColumnName="id", nullable=true)
	*/
    protected $documento;

	/**
	* @ORM\ManyToOne(targetEntity="Documento", inversedBy="chequespago",cascade={"persist"})
	* @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
	*/
    protected $pago;

	/**
	* @ORM\ManyToOne(targetEntity="Banco", inversedBy="cheques")
	* @ORM\JoinColumn(name="banco_id", referencedColumnName="id", nullable=true)
	*/
    protected $banco;

	/**
	* @ORM\ManyToOne(targetEntity="TipoCheque", inversedBy="cheques")
	* @ORM\JoinColumn(name="tipocheque_id", referencedColumnName="id", nullable=true)
	*/
    protected $tipocheque;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaemision
     *
     * @param \DateTime $fechaemision
     * @return Cheque
     */
    public function setFechaemision($fechaemision)
    {
        $this->fechaemision = $fechaemision;
    
        return $this;
    }

    /**
     * Get fechaemision
     *
     * @return \DateTime 
     */
    public function getFechaemision()
    {
        return $this->fechaemision;
    }

    /**
     * Set fechacobro
     *
     * @param \DateTime $fechacobro
     * @return Cheque
     */
    public function setFechacobro($fechacobro)
    {
        $this->fechacobro = $fechacobro;
    
        return $this;
    }

    /**
     * Get fechacobro
     *
     * @return \DateTime 
     */
    public function getFechacobro()
    {
        return $this->fechacobro;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Cheque
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set nrocheque
     *
     * @param string $nrocheque
     * @return Cheque
     */
    public function setNrocheque($nrocheque)
    {
        $this->nrocheque = $nrocheque;
    
        return $this;
    }

    /**
     * Get nrocheque
     *
     * @return string 
     */
    public function getNrocheque()
    {
        return $this->nrocheque;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     * @return Cheque
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;
    
        return $this;
    }

    /**
     * Get cuit
     *
     * @return string 
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set firmantes
     *
     * @param string $firmantes
     * @return Cheque
     */
    public function setFirmantes($firmantes)
    {
        $this->firmantes = $firmantes;
    
        return $this;
    }

    /**
     * Get firmantes
     *
     * @return string 
     */
    public function getFirmantes()
    {
        return $this->firmantes;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Cheque
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Cheque
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }

    /**
     * Set documento
     *
     * @param \JOYAS\JoyasBundle\Entity\Documento $documento
     * @return Cheque
     */
    public function setDocumento(\JOYAS\JoyasBundle\Entity\Documento $documento = null)
    {
        $this->documento = $documento;
    
        return $this;
    }

    /**
     * Get documento
     *
     * @return \JOYAS\JoyasBundle\Entity\Documento 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set banco
     *
     * @param \JOYAS\JoyasBundle\Entity\Banco $banco
     * @return Cheque
     */
    public function setBanco(\JOYAS\JoyasBundle\Entity\Banco $banco = null)
    {
        $this->banco = $banco;
    
        return $this;
    }

    /**
     * Get banco
     *
     * @return \JOYAS\JoyasBundle\Entity\Banco 
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * Set tipocheque
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoCheque $tipocheque
     * @return Cheque
     */
    public function setTipocheque(\JOYAS\JoyasBundle\Entity\TipoCheque $tipocheque = null)
    {
        $this->tipocheque = $tipocheque;
    
        return $this;
    }

    /**
     * Get tipocheque
     *
     * @return \JOYAS\JoyasBundle\Entity\TipoCheque 
     */
    public function getTipocheque()
    {
        return $this->tipocheque;
    }
    /**
     * Set documento
     *
     * @param \JOYAS\JoyasBundle\Entity\Documento $documento
     * @return Cheque
     */
    public function setPago(\JOYAS\JoyasBundle\Entity\Documento $pago = null)
    {
        $this->pago = $pago;
    
        return $this;
    }

    /**
     * Get documento
     *
     * @return \JOYAS\JoyasBundle\Entity\Documento 
     */
    public function getPago()
    {
        return $this->pago;
    }
    /**
     * Set conciliacion
     *
     * @param string $conciliacion
     * @return Cheque
     */
    public function setConciliacion($conciliacion)
    {
        $this->conciliacion = $conciliacion;
    
        return $this;
    }

    /**
     * Get conciliacion
     *
     * @return string 
     */
    public function getConciliacion()
    {
        return $this->conciliacion;
    }


    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Cheque
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}