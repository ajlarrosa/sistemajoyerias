<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ProductoDocumentoRepository")
 * @ORM\Table(name="productodocumento")
 */
class ProductoDocumento{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", length=100)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float", length=100)
     */
    protected $precio;

	/**
	* @ORM\ManyToOne(targetEntity="Producto", inversedBy="productosDocumento")
	* @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
	*/
    protected $producto;

	/**
	* @ORM\ManyToOne(targetEntity="Documento", inversedBy="productosDocumento", cascade={"persist"})
	* @ORM\JoinColumn(name="documento_id", referencedColumnName="id")
	*/
    protected $documento;
	
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return 'documento';
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return ProductoDocumento
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    
        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return ProductoDocumento
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    
        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ProductoDocumento
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $producto
     * @return ProductoDocumento
     */
    public function setProducto(\JOYAS\JoyasBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;
    
        return $this;
    }

    /**
     * Get producto
     *
     * @return \JOYAS\JoyasBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set documento
     *
     * @param \JOYAS\JoyasBundle\Entity\Documento $documento
     * @return ProductoDocumento
     */
    public function setDocumento(\JOYAS\JoyasBundle\Entity\Documento $documento = null)
    {
        $this->documento = $documento;
    
        return $this;
    }

    /**
     * Get documento
     *
     * @return \JOYAS\JoyasBundle\Entity\Documento 
     */
    public function getDocumento()
    {
        return $this->documento;
    }
}