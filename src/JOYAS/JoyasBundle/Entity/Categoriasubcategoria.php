<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JOYAS\JoyasBundle\Entity\TipoCosto;
use JOYAS\JoyasBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\CategoriasubcategoriaRepository")
 * @ORM\Table(name="categoriasubcategoria")
 */
class Categoriasubcategoria {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="categoriasubcategorias")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    protected $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="Subcategoria", inversedBy="categoriasubcategorias")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id")
     */
    protected $subcategoria;

    /**
     * @ORM\ManyToOne(targetEntity="Metal", inversedBy="categoriasubcategorias")
     * @ORM\JoinColumn(name="metal_id", referencedColumnName="id", nullable=true)
     */
    protected $metal;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="categoriasubcategoria")
     */
    protected $productos;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->categoria . ' (' . $this->subcategoria . ')';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ListaPrecio
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Categoriasubcategoria
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set categoria
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoria $categoria
     * @return Categoriasubcategoria
     */
    public function setCategoria(\JOYAS\JoyasBundle\Entity\Categoria $categoria = null) {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \JOYAS\JoyasBundle\Entity\Categoria 
     */
    public function getCategoria() {
        return $this->categoria;
    }

    /**
     * Set subcategoria
     *
     * @param \JOYAS\JoyasBundle\Entity\Subcategoria $subcategoria
     * @return Categoriasubcategoria
     */
    public function setSubcategoria(\JOYAS\JoyasBundle\Entity\Subcategoria $subcategoria = null) {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    /**
     * Get subcategoria
     *
     * @return \JOYAS\JoyasBundle\Entity\Subcategoria 
     */
    public function getSubcategoria() {
        return $this->subcategoria;
    }

    /**
     * Add productos
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $productos
     * @return Producto
     */
    public function addProducto(\JOYAS\JoyasBundle\Entity\Producto $productos) {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $productos
     */
    public function removePrecio(\JOYAS\JoyasBundle\Entity\Producto $productos) {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos() {
        return $this->productos;
    }

    /**
     * Remove productos
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $productos
     */
    public function removeProducto(\JOYAS\JoyasBundle\Entity\Producto $productos) {
        $this->productos->removeElement($productos);
    }


    /**
     * Set metal
     *
     * @param \JOYAS\JoyasBundle\Entity\Metal $metal
     * @return Categoriasubcategoria
     */
    public function setMetal(\JOYAS\JoyasBundle\Entity\Metal $metal = null)
    {
        $this->metal = $metal;
    
        return $this;
    }

    /**
     * Get metal
     *
     * @return \JOYAS\JoyasBundle\Entity\Metal 
     */
    public function getMetal()
    {
        return $this->metal;
    }
}