<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\PrecioRepository")
 * @ORM\Table(name="precio")
 * @UniqueEntity(
 * 		fields = {"producto","listaPrecio"},
 * 		message = "Ya existe un precio para este producto en esta lista"
 * 		)
 */
class Precio {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="precios")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id", nullable=false)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio", inversedBy="precios")
     * @ORM\JoinColumn(name="listaPrecio_id", referencedColumnName="id", nullable=false)
     */
    protected $listaPrecio;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     * 		message = "Debe poner un precio."
     * 		)
     */
    protected $valor;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'precio';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Precio
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Precio
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $producto
     * @return Precio
     */
    public function setProducto(\JOYAS\JoyasBundle\Entity\Producto $producto) {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \JOYAS\JoyasBundle\Entity\Producto 
     */
    public function getProducto() {
        return $this->producto;
    }

    /**
     * Set listaPrecio
     *
     * @param \JOYAS\JoyasBundle\Entity\ListaPrecio $listaPrecio
     * @return Precio
     */
    public function setListaPrecio(\JOYAS\JoyasBundle\Entity\ListaPrecio $listaPrecio) {
        $this->listaPrecio = $listaPrecio;

        return $this;
    }

    /**
     * Get listaPrecio
     *
     * @return \JOYAS\JoyasBundle\Entity\ListaPrecio 
     */
    public function getListaPrecio() {
        return $this->listaPrecio;
    }

}
