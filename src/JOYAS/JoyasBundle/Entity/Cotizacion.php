<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JOYAS\JoyasBundle\Entity\TipoCosto;
use JOYAS\JoyasBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\CotizacionRepository")
 * @ORM\Table(name="cotizacion")
 */
class Cotizacion{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $dolar;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $oro;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $plata;
    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $quilate;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $fecha;
     /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="cotizaciones")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=1,nullable=false)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->fecha = new \DateTime('NOW');
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dolar
     *
     * @param float $dolar
     * @return Cotizacion
     */
    public function setDolar($dolar)
    {
        $this->dolar = $dolar;
    
        return $this;
    }

    /**
     * Get dolar
     *
     * @return float 
     */
    public function getDolar()
    {
        return $this->dolar;
    }

    /**
     * Set oro
     *
     * @param float $oro
     * @return Cotizacion
     */
    public function setOro($oro)
    {
        $this->oro = $oro;
    
        return $this;
    }

    /**
     * Get oro
     *
     * @return float 
     */
    public function getOro()
    {
        return $this->oro;
    }

    /**
     * Set plata
     *
     * @param float $plata
     * @return Cotizacion
     */
    public function setPlata($plata)
    {
        $this->plata = $plata;
    
        return $this;
    }

    /**
     * Get plata
     *
     * @return float 
     */
    public function getPlata()
    {
        return $this->plata;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     * @return Cotizacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return string 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Cotizacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Cotizacion
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }

    /**
     * Set quilate
     *
     * @param float $quilate
     * @return Cotizacion
     */
    public function setQuilate($quilate)
    {
        $this->quilate = $quilate;
    
        return $this;
    }

    /**
     * Get quilate
     *
     * @return float 
     */
    public function getQuilate()
    {
        return $this->quilate;
    }
}