<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\PedidoRepository")
 * @ORM\Table(name="pedido")
 */
class Pedido{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
		
	/**
	* @ORM\ManyToOne(targetEntity="UsuarioWeb", inversedBy="pedidos")
	* @ORM\JoinColumn(name="usuarioweb_id", referencedColumnName="id")
	*/
    protected $usuarioweb;

    /**
	 * @ORM\Column(type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $formapago;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $idpago;

	/**
	* @ORM\OneToMany(targetEntity="DetallePedido", mappedBy="pedido", cascade={"persist", "remove"} )
	*/
	protected $detallespedido;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/ 
	public function __construct()
	{
		$this->detallespedido = new ArrayCollection();
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pedido
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Pedido
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Pedido
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    
        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set formapago
     *
     * @param string $formapago
     * @return Pedido
     */
    public function setFormapago($formapago)
    {
        $this->formapago = $formapago;
    
        return $this;
    }

    /**
     * Get formapago
     *
     * @return string 
     */
    public function getFormapago()
    {
        return $this->formapago;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Pedido
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set usuarioweb
     *
     * @param \JOYAS\JoyasBundle\Entity\UsuarioWeb $usuarioweb
     * @return Pedido
     */
    public function setUsuarioweb(\JOYAS\JoyasBundle\Entity\UsuarioWeb $usuarioweb = null)
    {
        $this->usuarioweb = $usuarioweb;
    
        return $this;
    }

    /**
     * Get usuarioweb
     *
     * @return \JOYAS\JoyasBundle\Entity\UsuarioWeb 
     */
    public function getUsuarioweb()
    {
        return $this->usuarioweb;
    }

    /**
     * Add detallespedido
     *
     * @param \JOYAS\JoyasBundle\Entity\DetallePedido $detallespedido
     * @return Pedido
     */
    public function addDetallespedido(\JOYAS\JoyasBundle\Entity\DetallePedido $detallespedido)
    {
        $this->detallespedido[] = $detallespedido;
    
        return $this;
    }

    /**
     * Remove detallespedido
     *
     * @param \JOYAS\JoyasBundle\Entity\DetallePedido $detallespedido
     */
    public function removeDetallespedido(\JOYAS\JoyasBundle\Entity\DetallePedido $detallespedido)
    {
        $this->detallespedido->removeElement($detallespedido);
    }

    /**
     * Get detallespedido
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDetallespedido()
    {
        return $this->detallespedido;
    }

    /**
     * Set idpago
     *
     * @param string $idpago
     * @return Pedido
     */
    public function setIdpago($idpago)
    {
        $this->idpago = $idpago;
    
        return $this;
    }

    /**
     * Get idpago
     *
     * @return string 
     */
    public function getIdpago()
    {
        return $this->idpago;
    }
}