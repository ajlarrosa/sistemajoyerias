<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\DocumentoRepository")
 * @ORM\Table(name="documento")
 */
class Documento {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCC")
     * @ORM\JoinColumn(name="movimientocc_id", referencedColumnName="id", nullable=true)
     * */
    private $movimientocc;

    /**
     * @ORM\ManyToOne(targetEntity="TipoGasto", inversedBy="documentos")
     * @ORM\JoinColumn(name="tipoGasto_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoGasto;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaRegistracion;

    /**
     * @ORM\Column(type="float", length=100, nullable=false)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $efectivo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nrocomprobante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nrofactura;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nroremito;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion;

    /**
     * @ORM\Column(type="string", length=2, nullable=false)
     */
    protected $tipoDocumento;

    /**
     * @ORM\Column(type="string", length=3, nullable=false)
     */
    protected $moneda;

    /**
     * @ORM\OneToMany(targetEntity="ProductoDocumento", mappedBy="documento", cascade={"persist"})
     */
    protected $productosDocumento;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="documento", cascade={"persist"})
     */
    protected $cheques;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="pago", cascade={"persist"})
     */
    protected $chequespago;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nroreciboproveedor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechareciboproveedor;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $oro;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $plata;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosDocumento = new ArrayCollection();
        $this->cheques = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Documento
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return Documento
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set tipoDocumento
     *
     * @param string $tipoDocumento
     * @return Documento
     */
    public function setTipoDocumento($tipoDocumento) {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return string 
     */
    public function getTipoDocumento() {
        return $this->tipoDocumento;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     * @return Documento
     */
    public function setMoneda($moneda) {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMoneda() {
        return $this->moneda;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMonedaStr() {
        if ($this->moneda == 1) {
            return '$';
        }
        if ($this->moneda == 2) {
            return 'u$s';
        }
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Documento
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set tipoGasto
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoGasto $tipoGasto
     * @return Documento
     */
    public function setTipoGasto(\JOYAS\JoyasBundle\Entity\TipoGasto $tipoGasto = null) {
        $this->tipoGasto = $tipoGasto;

        return $this;
    }

    /**
     * Get tipoGasto
     *
     * @return \JOYAS\JoyasBundle\Entity\TipoGasto 
     */
    public function getTipoGasto() {
        return $this->tipoGasto;
    }

    /**
     * Set fechaRegistracion
     *
     * @param \DateTime $fechaRegistracion
     * @return Documento
     */
    public function setFechaRegistracion($fechaRegistracion) {
        $this->fechaRegistracion = $fechaRegistracion;

        return $this;
    }

    /**
     * Get fechaRegistracion
     *
     * @return \DateTime 
     */
    public function getFechaRegistracion() {
        return $this->fechaRegistracion;
    }

    /**
     * Set movimientocc
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoCC $movimientocc
     * @return Documento
     */
    public function setMovimientocc(\JOYAS\JoyasBundle\Entity\MovimientoCC $movimientocc = null) {
        $this->movimientocc = $movimientocc;

        return $this;
    }

    /**
     * Get movimientocc
     *
     * @return \JOYAS\JoyasBundle\Entity\MovimientoCC 
     */
    public function getMovimientocc() {
        return $this->movimientocc;
    }

    /**
     * Add productosDocumento
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento
     * @return Documento
     */
    public function addProductosDocumento(\JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento) {
        $this->productosDocumento[] = $productosDocumento;

        return $this;
    }

    /**
     * Remove productosDocumento
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento
     */
    public function removeProductosDocumento(\JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento) {
        $this->productosDocumento->removeElement($productosDocumento);
    }

    /**
     * Get productosDocumento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosDocumento() {
        return $this->productosDocumento;
    }
    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Documento
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Documento
     */
    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float 
     */
    public function getBonificacion() {
        return $this->bonificacion;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Documento
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

   
    /**
     * Set nrocomprobante
     *
     * @param string $nrocomprobante
     * @return Documento
     */
    public function setNrocomprobante($nrocomprobante) {
        $this->nrocomprobante = $nrocomprobante;

        return $this;
    }

    /**
     * Get nrocomprobante
     *
     * @return string 
     */
    public function getNrocomprobante() {
        return $this->nrocomprobante;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Documento
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion() {
        return $this->observacion;
    }

    /**
     * Add cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     * @return Documento
     */
    public function addCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques) {
        $this->cheques[] = $cheques;

        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques) {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheques() {
        return $this->cheques;
    }

    /**
     * Set nrocomprobante
     *
     * @param string $nrofactura
     * @return Documento
     */
    public function setNrofactura($nrofactura) {
        $this->nrofactura = $nrofactura;

        return $this;
    }

    /**
     * Get nrofactura
     *
     * @return string 
     */
    public function getNrofactura() {
        return $this->nrofactura;
    }

    /**
     * Set nroremito
     *
     * @param string $nroremito
     * @return Documento
     */
    public function setNroremito($nroremito) {
        $this->nroremito = $nroremito;

        return $this;
    }

    /**
     * Get nroremito
     *
     * @return string 
     */
    public function getNroremito() {
        return $this->nroremito;
    }

    /**
     * Add chequespago
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque chequespago
     * @return Documento
     */
    public function addChequepago(\JOYAS\JoyasBundle\Entity\Cheque $chequespago) {
        $this->chequespago[] = $chequespago;

        return $this;
    }

    /**
     * Remove chequespago
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $chequespago
     */
    public function removeChequepago(\JOYAS\JoyasBundle\Entity\Cheque $chequespago) {
        $this->chequespago->removeElement($chequespago);
    }

    /**
     * Get chequespago
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChequespago() {
        return $this->chequespago;
    }

    /**
     * Set nroreciboproveedor
     *
     * @param string $nroreciboproveedor
     * @return Documento
     */
    public function setNroreciboproveedor($nroreciboproveedor) {
        $this->nroreciboproveedor = $nroreciboproveedor;

        return $this;
    }

    /**
     * Get nroreciboproveedor
     *
     * @return string 
     */
    public function getNroreciboproveedor() {
        return $this->nroreciboproveedor;
    }

    /**
     * Set fechareciboproveedor
     *
     * @param \DateTime $fechareciboproveedor
     * @return Documento
     */
    public function setFechareciboproveedor($fechareciboproveedor) {
        $this->fechareciboproveedor = $fechareciboproveedor;

        return $this;
    }

    /**
     * Get fechareciboproveedor
     *
     * @return \DateTime 
     */
    public function getFechareciboproveedor() {
        return $this->fechareciboproveedor;
    }

    /**
     * Add chequespago
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $chequespago
     * @return Documento
     */
    public function addChequespago(\JOYAS\JoyasBundle\Entity\Cheque $chequespago) {
        $this->chequespago[] = $chequespago;

        return $this;
    }

    /**
     * Remove chequespago
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $chequespago
     */
    public function removeChequespago(\JOYAS\JoyasBundle\Entity\Cheque $chequespago) {
        $this->chequespago->removeElement($chequespago);
    }

    /**
     * Set efectivo
     *
     * @param string $efectivo
     * @return Documento
     */
    public function setEfectivo($efectivo) {
        $this->efectivo = $efectivo;

        return $this;
    }

    /**
     * Get efectivo
     *
     * @return string 
     */
    public function getEfectivo() {
        return $this->efectivo;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMonedaSim() {
        if ($this->moneda == 1) {
            return '$';
        }
        if ($this->moneda == 2) {
            return 'u$s';
        }
    }

    /**
     * Set oro
     *
     * @param string $oro
     * @return Documento
     */
    public function setOro($oro) {
        $this->oro = $oro;

        return $this;
    }

    /**
     * Get oro
     *
     * @return string 
     */
    public function getOro() {
        return $this->oro;
    }

    /**
     * Set plata
     *
     * @param string $plata
     * @return Documento
     */
    public function setPlata($plata) {
        $this->plata = $plata;

        return $this;
    }

    /**
     * Get plata
     *
     * @return string 
     */
    public function getPlata() {
        return $this->plata;
    }

    public function getPlataString() {
        if (isset($this->plata)) {
            return $this->plata . ' gr';
        } else {
            return '-';
        }
    }

    public function getOroString() {
        if (isset($this->oro)) {
            return $this->oro . ' gr';
        } else {
            return '-';
        }
    }

    public function getImporteString() {
        if (isset($this->importe)) {
            if ($this->importe < 0) {
                return ' - ' . $this->getMonedaStr() . ' ' . variant_abs($this->importe);
            } else {
                return $this->getMonedaStr() . ' ' . $this->importe;
            }
        } else {
            return '-';
        }
    }

}