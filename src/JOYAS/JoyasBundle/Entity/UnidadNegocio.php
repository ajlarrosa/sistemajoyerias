<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\UnidadNegocioRepository")
 * @ORM\Table(name="unidadnegocio")
 */
class UnidadNegocio {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $responsable;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $direccion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $celular;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $mail;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="unidadNegocio")
     */
    protected $productos;

    /**
     * @ORM\OneToMany(targetEntity="ListaPrecio", mappedBy="unidadNegocio")
     */
    protected $listasprecio;

    /**
     * @ORM\OneToMany(targetEntity="Usuario", mappedBy="unidadNegocio")
     */
    protected $usuarios;

    /**
     * @ORM\OneToMany(targetEntity="ClienteProveedor", mappedBy="unidadNegocio")
     */
    protected $clientesproveedores;

    /**
     * @ORM\OneToMany(targetEntity="MovimientoCC", mappedBy="unidadNegocio")
     */
    protected $movimientoscc;

    /**
     * @ORM\OneToMany(targetEntity="Consignacion", mappedBy="unidadNegocio")
     */
    protected $consignaciones;
    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="unidadNegocio")
     */
    protected $cheques;
    /**
     * @ORM\OneToMany(targetEntity="Categoria", mappedBy="unidadNegocio")
     */
    protected $categorias;
    /**
     * @ORM\OneToMany(targetEntity="Subcategoria", mappedBy="unidadNegocio")
     */
    protected $subcategorias;
    /**
     * @ORM\OneToMany(targetEntity="Banco", mappedBy="unidadNegocio")
     */
    protected $bancos;
    /**
     * @ORM\OneToMany(targetEntity="TipoCheque", mappedBy="unidadNegocio")
     */
    protected $tiposCheques;
    /**
     * @ORM\OneToMany(targetEntity="TipoGasto", mappedBy="unidadNegocio")
     */
    protected $tiposGastos;
    /**
     * @ORM\OneToMany(targetEntity="Cotizacion", mappedBy="unidadNegocio")
     */
    protected $cotizaciones;
     /**
     * @ORM\OneToMany(targetEntity="NumeracionRecibo", mappedBy="unidadNegocio")
     */
    protected $numeracionrecibo;

    
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';
    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    protected $usaParametrica = false;
    

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productos = new ArrayCollection();
        $this->usuarios = new ArrayCollection();
        $this->listasprecio = new ArrayCollection();
        $this->clientesproveedores = new ArrayCollection();
        $this->movimientoscc = new ArrayCollection();
        $this->consignaciones = new ArrayCollection();
        $this->cheques = new ArrayCollection();
        $this->categorias = new ArrayCollection();
        $this->subcategorias = new ArrayCollection();
        $this->bancos = new ArrayCollection();
        $this->tiposCheques = new ArrayCollection();
        $this->tiposGastos = new ArrayCollection();
        $this->cotizaciones = new ArrayCollection();
        $this->numeracionrecibo = new ArrayCollection();        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return UnidadNegocio
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return UnidadNegocio
     */
    public function setResponsable($responsable) {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string 
     */
    public function getResponsable() {
        return $this->responsable;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return UnidadNegocio
     */
    public function setDireccion($direccion) {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion() {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return UnidadNegocio
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return UnidadNegocio
     */
    public function setCelular($celular) {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular() {
        return $this->celular;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return UnidadNegocio
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return UnidadNegocio
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add productos
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $productos
     * @return UnidadNegocio
     */
    public function addProducto(\JOYAS\JoyasBundle\Entity\Producto $productos) {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $productos
     */
    public function removeProducto(\JOYAS\JoyasBundle\Entity\Producto $productos) {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos() {
        return $this->productos;
    }

    /**
     * Add listasprecio
     *
     * @param \JOYAS\JoyasBundle\Entity\ListaPrecio $listasprecio
     * @return UnidadNegocio
     */
    public function addListasprecio(\JOYAS\JoyasBundle\Entity\ListaPrecio $listasprecio) {
        $this->listasprecio[] = $listasprecio;

        return $this;
    }

    /**
     * Remove listasprecio
     *
     * @param \JOYAS\JoyasBundle\Entity\ListaPrecio $listasprecio
     */
    public function removeListasprecio(\JOYAS\JoyasBundle\Entity\ListaPrecio $listasprecio) {
        $this->listasprecio->removeElement($listasprecio);
    }

    /**
     * Get listasprecio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getListasprecio() {
        return $this->listasprecio;
    }

    /**
     * Add usuarios
     *
     * @param \JOYAS\JoyasBundle\Entity\Usuario $usuarios
     * @return UnidadNegocio
     */
    public function addUsuario(\JOYAS\JoyasBundle\Entity\Usuario $usuarios) {
        $this->usuarios[] = $usuarios;

        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \JOYAS\JoyasBundle\Entity\Usuario $usuarios
     */
    public function removeUsuario(\JOYAS\JoyasBundle\Entity\Usuario $usuarios) {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios() {
        return $this->usuarios;
    }

    /**
     * Add clientesproveedores
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores
     * @return UnidadNegocio
     */
    public function addClientesproveedore(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores) {
        $this->clientesproveedores[] = $clientesproveedores;

        return $this;
    }

    /**
     * Remove clientesproveedores
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores
     */
    public function removeClientesproveedore(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores) {
        $this->clientesproveedores->removeElement($clientesproveedores);
    }

    /**
     * Get clientesproveedores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientesproveedores() {
        return $this->clientesproveedores;
    }

    /**
     * Add movimientoscc
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc
     * @return UnidadNegocio
     */
    public function addMovimientoscc(\JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc) {
        $this->movimientoscc[] = $movimientoscc;

        return $this;
    }

    /**
     * Remove movimientoscc
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc
     */
    public function removeMovimientoscc(\JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc) {
        $this->movimientoscc->removeElement($movimientoscc);
    }

    /**
     * Get movimientoscc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientoscc() {
        return $this->movimientoscc;
    }


    /**
     * Add consignaciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Consignacion $consignaciones
     * @return UnidadNegocio
     */
    public function addConsignacione(\JOYAS\JoyasBundle\Entity\Consignacion $consignaciones)
    {
        $this->consignaciones[] = $consignaciones;
    
        return $this;
    }

    /**
     * Remove consignaciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Consignacion $consignaciones
     */
    public function removeConsignacione(\JOYAS\JoyasBundle\Entity\Consignacion $consignaciones)
    {
        $this->consignaciones->removeElement($consignaciones);
    }

    /**
     * Get consignaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsignaciones()
    {
        return $this->consignaciones;
    }

    /**
     * Add cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     * @return UnidadNegocio
     */
    public function addCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques)
    {
        $this->cheques[] = $cheques;
    
        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques)
    {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheques()
    {
        return $this->cheques;
    }

    /**
     * Set usaParametrica
     *
     * @param boolean $usaParametrica
     * @return UnidadNegocio
     */
    public function setUsaParametrica($usaParametrica)
    {
        $this->usaParametrica = $usaParametrica;
    
        return $this;
    }

    /**
     * Get usaParametrica
     *
     * @return boolean 
     */
    public function getUsaParametrica()
    {
        return $this->usaParametrica;
    }

    /**
     * Add categorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoria $categorias
     * @return UnidadNegocio
     */
    public function addCategoria(\JOYAS\JoyasBundle\Entity\Categoria $categorias)
    {
        $this->categorias[] = $categorias;
    
        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\JOYAS\JoyasBundle\Entity\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Add subcategorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoria $subcategorias
     * @return UnidadNegocio
     */
    public function addSubcategoria(\JOYAS\JoyasBundle\Entity\Categoria $subcategorias)
    {
        $this->subcategorias[] = $subcategorias;
    
        return $this;
    }

    /**
     * Remove subcategorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoria $subcategorias
     */
    public function removeSubcategoria(\JOYAS\JoyasBundle\Entity\Categoria $subcategorias)
    {
        $this->subcategorias->removeElement($subcategorias);
    }

    /**
     * Get subcategorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubcategorias()
    {
        return $this->subcategorias;
    }

    /**
     * Add bancos
     *
     * @param \JOYAS\JoyasBundle\Entity\Banco $bancos
     * @return UnidadNegocio
     */
    public function addBanco(\JOYAS\JoyasBundle\Entity\Banco $bancos)
    {
        $this->bancos[] = $bancos;
    
        return $this;
    }

    /**
     * Remove bancos
     *
     * @param \JOYAS\JoyasBundle\Entity\Banco $bancos
     */
    public function removeBanco(\JOYAS\JoyasBundle\Entity\Banco $bancos)
    {
        $this->bancos->removeElement($bancos);
    }

    /**
     * Get bancos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBancos()
    {
        return $this->bancos;
    }

    /**
     * Add tiposCheques
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoCheque $tiposCheques
     * @return UnidadNegocio
     */
    public function addTiposCheque(\JOYAS\JoyasBundle\Entity\TipoCheque $tiposCheques)
    {
        $this->tiposCheques[] = $tiposCheques;
    
        return $this;
    }

    /**
     * Remove tiposCheques
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoCheque $tiposCheques
     */
    public function removeTiposCheque(\JOYAS\JoyasBundle\Entity\TipoCheque $tiposCheques)
    {
        $this->tiposCheques->removeElement($tiposCheques);
    }

    /**
     * Get tiposCheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTiposCheques()
    {
        return $this->tiposCheques;
    }

    /**
     * Add tiposGastos
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos
     * @return UnidadNegocio
     */
    public function addTiposGsto(\JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos)
    {
        $this->tiposGastos[] = $tiposGastos;
    
        return $this;
    }

    /**
     * Remove tiposGastos
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos
     */
    public function removeTiposGsto(\JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos)
    {
        $this->tiposGastos->removeElement($tiposGastos);
    }

    /**
     * Get tiposGastos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTiposGstos()
    {
        return $this->tiposGastos;
    }

    /**
     * Add cotizaciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Cotizacion $cotizaciones
     * @return UnidadNegocio
     */
    public function addCotizacione(\JOYAS\JoyasBundle\Entity\Cotizacion $cotizaciones)
    {
        $this->cotizaciones[] = $cotizaciones;
    
        return $this;
    }

    /**
     * Remove cotizaciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Cotizacion $cotizaciones
     */
    public function removeCotizacione(\JOYAS\JoyasBundle\Entity\Cotizacion $cotizaciones)
    {
        $this->cotizaciones->removeElement($cotizaciones);
    }

    /**
     * Get cotizaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCotizaciones()
    {
        return $this->cotizaciones;
    }

    /**
     * Add tiposGastos
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos
     * @return UnidadNegocio
     */
    public function addTiposGasto(\JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos)
    {
        $this->tiposGastos[] = $tiposGastos;
    
        return $this;
    }

    /**
     * Remove tiposGastos
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos
     */
    public function removeTiposGasto(\JOYAS\JoyasBundle\Entity\TipoGasto $tiposGastos)
    {
        $this->tiposGastos->removeElement($tiposGastos);
    }

    /**
     * Get tiposGastos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTiposGastos()
    {
        return $this->tiposGastos;
    }

    /**
     * Add numeracionrecibo
     *
     * @param \JOYAS\JoyasBundle\Entity\NumeracionRecibo $numeracionrecibo
     * @return UnidadNegocio
     */
    public function addNumeracionrecibo(\JOYAS\JoyasBundle\Entity\NumeracionRecibo $numeracionrecibo)
    {
        $this->numeracionrecibo[] = $numeracionrecibo;
    
        return $this;
    }

    /**
     * Remove numeracionrecibo
     *
     * @param \JOYAS\JoyasBundle\Entity\NumeracionRecibo $numeracionrecibo
     */
    public function removeNumeracionrecibo(\JOYAS\JoyasBundle\Entity\NumeracionRecibo $numeracionrecibo)
    {
        $this->numeracionrecibo->removeElement($numeracionrecibo);
    }

    /**
     * Get numeracionrecibo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNumeracionrecibo()
    {
        return $this->numeracionrecibo;
    }
}