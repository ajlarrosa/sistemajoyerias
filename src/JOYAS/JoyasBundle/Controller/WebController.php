<?php

namespace JOYAS\JoyasBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;

class WebController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * @var MailsManager
     * @DI\Inject("mails.manager")
     */
    public $mail;

    public function indexAction() {
        if (session_status() == 'PHP_SESSION_DISABLED') {
            $this->sessionSvc->startSession();
        }

        $novedades = $this->obtenerNovedades();
        $destacados = $this->obtenerDestacados();
        $banners = $this->obtenerBanners();

        return $this->render('JOYASJoyasBundle:Web:index.html.twig', array(
                    'novedades' => $novedades,
                    'destacados' => $destacados,
                    'banners' => $banners
        ));
    }

    public function catalogoAction(Request $request, $page = 1) {
        if (session_status() == 'PHP_SESSION_DISABLED') {
            $this->sessionSvc->startSession();
        }
        $categoria = $request->request->get('categoria');
        $busqueda = $request->request->get('busqueda');
        $filter = $request->request->get('filter'); 
       
        $destacados = $this->obtenerDestacados();
        $banners = $this->obtenerBanners();

        $em = $this->getDoctrine()->getManager();        
        if ($page==1){
            $offset=0;
        }else{
            $offset = ($page - 1) * 32;
        }    
        $productos = $em->getRepository('JOYASJoyasBundle:Producto')->obtenerProductos($categoria, $offset, 32, $busqueda, $filter);
        $categorias = $em->getRepository('JOYASJoyasBundle:Categoria')->getCategoriasWeb();
        $categoriaActual = null;
        if (isset($categoria) && $categoria != '') {
            $categoriaActual = $em->getRepository('JOYASJoyasBundle:Categoria')->find($categoria);
        }

        return $this->render('JOYASJoyasBundle:Web:catalogo.html.twig', array(
                    'productos' => $productos,
                    'destacados' => $destacados,
                    'banners' => $banners,
                    'paginaAnterior' => $this->obtenerAnterior($page),
                    'paginaSiguiente' => $this->obtenerSiguiente($page),
                    'categorias' => $categorias,
                    'categoriaActual' => $categoriaActual,
                    'filtroActual' => $filter,
                    'busqueda' => $busqueda,                  
                    'page' => $page
        ));
    }

    private function obtenerAnterior($page) {
        if ($page == 1) {
            return 1;
        } else {
            return $page - 1;
        }
    }

    private function obtenerSiguiente($page) {
        return $page + 1;       
    }

    public function singleAction($id, $descripcion) {
        if (session_status() == 'PHP_SESSION_DISABLED') {
            $this->sessionSvc->startSession();
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('id' => $id, 'unidadNegocio' => 1, 'visible' => true, 'estado' => 'A'));
        if (!$entity) {
            $this->sessionSvc->addFlash('msgError', 'No se ha podido mostrar el producto seleccionado.');
            return $this->redirect($this->generateUrl('joyas_joyas_web_index'));
        }
        $destacados = $this->obtenerDestacados();
        $banners = $this->obtenerBanners();
        return $this->render('JOYASJoyasBundle:Web:single.html.twig', array(
                    'entity' => $entity,
                    'destacados' => $destacados,
                    'banners' => $banners
        ));
    }

    public function mantenimientoAction() {
        return $this->render('JOYASJoyasBundle:Web:mantenimiento.html.twig');
    }

    private function obtenerBanners() {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('JOYASJoyasBundle:Imagen')->findBy(array('estado' => 'A'));
    }

    private function obtenerNovedades() {
        $em = $this->getDoctrine()->getManager();

        $cuenta = $em->getRepository('JOYASJoyasBundle:Producto')->getNovedadesCount();
        if ($cuenta > 4) {
            $cuenta = $cuenta - 4;
            //Traer un numero aleatorio entre 0 y el tamano total de categorias - 5        
            $inicio = rand(0, $cuenta);
        } else {
            $inicio = 0;
        }
        return $em->getRepository('JOYASJoyasBundle:Producto')->getNovedades($inicio, 4);
    }

    private function obtenerDestacados() {
        $em = $this->getDoctrine()->getManager();

        $cuenta = $em->getRepository('JOYASJoyasBundle:Producto')->getDestacadosCount();
        if ($cuenta > 8) {
            $cuenta = $cuenta - 8;
            //Traer un numero aleatorio entre 0 y el tamano total de categorias - 5        
            $inicio = rand(0, $cuenta);
        } else {
            $inicio = 0;
        }
        return $em->getRepository('JOYASJoyasBundle:Producto')->getDestacados($inicio, 8);
    }

    public function contactoAction() {
        $destacados = $this->obtenerDestacados();
        $banners = $this->obtenerBanners();
        return $this->render('JOYASJoyasBundle:Web:contact.html.twig', array(
                    'destacados' => $destacados,
                    'banners' => $banners
        ));
    }

    public function contactoEmailAction(Request $request) {
        if (session_status() == 'PHP_SESSION_DISABLED') {
            $this->sessionSvc->startSession();
        }
        $datos =new \stdClass();
        $datos->nombre =$request->request->get('name');
        $datos->email= $request->request->get('email');
        $datos->website = $request->request->get('website');
        $datos->telefono = $request->request->get('telefono');
        $datos->titulo = $request->request->get('subject');
        $datos->mensaje = $request->request->get('comments');
        
        if(trim($datos->mensaje)=='' || trim($datos->titulo)==''){
            $this->sessionSvc->addFlash('msgError', 'No se ha podido enviar la consulta el título y el mensaje son obligatorios.');
        } else  if (!$this->mail->enviarMail($datos) === TRUE) {
            $this->sessionSvc->addFlash('msgError', 'No se ha podido enviar la consulta en este momento.');
        }else {
            $this->sessionSvc->addFlash('msgError', 'Se ha enviado la consulta. En breve nos estaremos poniendo en contacto con usted.');
        }
        
        return $this->redirect($this->generateUrl('joyas_joyas_web_contacto'));
    }
    public function nosotrosAction() {
        $destacados = $this->obtenerDestacados();
        $banners = $this->obtenerBanners();
        return $this->render('JOYASJoyasBundle:Web:nosotros.html.twig', array(
                    'destacados' => $destacados,
                    'banners' => $banners
        ));
    }

}

?>
