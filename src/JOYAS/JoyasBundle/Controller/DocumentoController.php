<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\Documento;
use JOYAS\JoyasBundle\Entity\Producto;
use JOYAS\JoyasBundle\Entity\Cheque;
use JOYAS\JoyasBundle\Entity\ProductoDocumento;
use JOYAS\JoyasBundle\Entity\NumeracionRecibo;
use JOYAS\JoyasBundle\Entity\MovimientoCC;
use JOYAS\JoyasBundle\Form\DocumentoType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Documento controller.
 *
 */
class DocumentoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Documento entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Documento')->findAll();

        return $this->render('JOYASJoyasBundle:Documento:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function filtroAction(Request $request, $place) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $this->sessionSvc->setSession('unidaddenegocio', $request->get('unidadnegocio'));

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $documentos = $em->getRepository('JOYASJoyasBundle:Documento')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            if ($request->get('unidadnegocio') != '0') {
                $documentos = $em->getRepository('JOYASJoyasBundle:Documento')->getAllActivas($request->get('unidadnegocio'));
            } else {
                $documentos = $em->getRepository('JOYASJoyasBundle:Documento')->getAllActivas();
            }
        }

        $entities = new ArrayCollection();

        $desde = new \DateTime($request->get('fechaDesde'));
        $hasta = new \DateTime($request->get('fechaHasta'));
        $listado = $request->get('listado');

        foreach ($documentos as $doc) {
            $fecha = $doc->getFecha();

            if ($request->get('fechaDesde') != '' and $request->get('fechaHasta') != '') {
                if ($desde <= $fecha and $doc->getFecha() <= $hasta) {
                    if ($listado == '0') {
                        $entities->add($doc);
                    } else {
                        if ($place != 'gastos' and $doc->getMovimientoCC()->getTipoDocumento() != 'G'and $doc->getMovimientoCC()->getTipoDocumento() != 'A' and $doc->getMovimientoCC()->getTipoDocumento() != 'FC') {
                            if ($listado == $doc->getMovimientoCC()->getClienteProveedor()->getId()) {
                                $entities->add($doc);
                            }
                        } else {
                            if ($place == 'gastos' and $doc->getMovimientoCC()->getTipoDocumento() == 'G') {
                                if ($listado == $doc->getTipoGasto()->getId()) {
                                    $entities->add($doc);
                                }
                            }
                        }
                    }
                }
            } else {
                if ($listado != '0') {
                    if ($place != 'gastos' and $doc->getMovimientoCC()->getTipoDocumento() != 'G' and $doc->getMovimientoCC()->getTipoDocumento() != 'FC' and $doc->getMovimientoCC()->getTipoDocumento() != 'A') {
                        if ($listado == $doc->getMovimientoCC()->getClienteProveedor()->getId()) {
                            $entities->add($doc);
                        }
                    } else {
                        if ($place == 'gastos' and $doc->getMovimientoCC()->getTipoDocumento() == 'G') {
                            if ($listado == $doc->getTipoGasto()->getId()) {
                                $entities->add($doc);
                            }
                        }
                    }
                }
            }
        }

        if ($request->get('unidadnegocio') != '0' and $listado == '0' and $request->get('fechaDesde') == '' and $request->get('fechaHasta') == '' and count($entities) < 1 and count($documentos) > 0) {
            $entities = $documentos;
        }

        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            if ($request->get('unidadnegocio') == '0' and $listado == '0' and $request->get('fechaDesde') == '' and $request->get('fechaHasta') == '' and count($entities) < 1 and count($documentos) > 0) {
                $entities = $em->getRepository('JOYASJoyasBundle:Documento')->getAllActivas();
            }
        }

        if ($place == 'compras') {

            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                //$query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\ClienteProveedor e WHERE e.estado = :activo AND e.clienteProveedor = 2 AND e.unidadNegocio = :unidad ORDER BY e.razonSocial ASC')->setParameter(':activo', 'A')->setParameter(':unidad', $unidad);
                //$clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllProv($this->sessionSvc->getSession('unidad'));
                $proveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('unidadNegocio'=>$this->sessionSvc->getSession('unidad'),'estado'=>'A','clienteProveedor'=>2), array('razonSocial' => 'ASC'));
            } else {
                //$clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllProv();
                $proveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('estado'=>'A','clienteProveedor'=>2), array('razonSocial' => 'ASC'));
            }

            return $this->render('JOYASJoyasBundle:Documento:compras.html.twig', array(
                        'entities' => $entities,
                        'unidades' => $unidades,
                        'proveedores' => $proveedores,
            ));
        }
        if ($place == 'cobranzas') {
            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($this->sessionSvc->getSession('unidad'));
            } else {
                $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
            }

            return $this->render('JOYASJoyasBundle:Documento:cobranzas.html.twig', array(
                        'entities' => $entities,
                        'unidades' => $unidades,
                        'clientesProveedores' => $clientesProveedores,
            ));
        }
        if ($place == 'gastos') {
            $tiposGasto = $em->getRepository('JOYASJoyasBundle:TipoGasto')->getAllActivas();

            return $this->render('JOYASJoyasBundle:Documento:gastos.html.twig', array(
                        'entities' => $entities,
                        'unidades' => $unidades,
                        'tiposGasto' => $tiposGasto,
            ));
        }
        if ($place == 'pagos') {
            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllProv($this->sessionSvc->getSession('unidad'));
            } else {
                $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllProv();
            }


            return $this->render('JOYASJoyasBundle:Documento:pagos.html.twig', array(
                        'entities' => $entities,
                        'unidades' => $unidades,
                        'clientesProveedores' => $clientesProveedores,
            ));
        }
    }

    public function comprasAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $unidadActual = $this->sessionSvc->getSession('unidad');
        $unidad = $request->get('unidadNegocio');
        $cliente = $request->get('cliente');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bCliente', $cliente);
        $this->sessionSvc->setSession('bFechaDesde', $fechaDesde);
        $this->sessionSvc->setSession('bFechaHasta', $fechaHasta);

        $em = $this->getDoctrine()->getManager();
        $tipoDocumento = 'F';
        $entities = $em->getRepository('JOYASJoyasBundle:Documento')->filtroBusqueda($unidad, $fechaDesde, $fechaHasta, $cliente, $unidadActual, $tipoDocumento);

        if (isset($unidadActual) && $unidadActual != '') {
            //$clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($this->sessionSvc->getSession('unidad'));
            $proveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('unidadNegocio' => $this->sessionSvc->getSession('unidad'), 'clienteProveedor' => 2,'estado'=>'A'), array('razonSocial' => 'ASC'));
        } else {
            //$clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
            $proveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor' => 2,'estado'=>'A'), array('razonSocial' => 'ASC'));
        }
      
        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Documento:compras.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,                  
                    'proveedores' => $proveedores
        ));
    }

    public function cobranzasAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $unidadActual = $this->sessionSvc->getSession('unidad');
        $unidad = $request->get('unidadNegocio');
        $cliente = $request->get('cliente');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bCliente', $cliente);
        $this->sessionSvc->setSession('bFechaDesde', $fechaDesde);
        $this->sessionSvc->setSession('bFechaHasta', $fechaHasta);

        $em = $this->getDoctrine()->getManager();
        $tipoDocumento = 'R';
        $entities = $em->getRepository('JOYASJoyasBundle:Documento')->filtroBusqueda($unidad, $fechaDesde, $fechaHasta, $cliente, $unidadActual, $tipoDocumento);

        if (isset($unidadActual) && $unidadActual != '') {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($this->sessionSvc->getSession('unidad'));
        } else {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
        }
        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Documento:cobranzas.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
                    'clientesProveedores' => $clientesProveedores,
        ));
    }

    public function pagosAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $unidadActual = $this->sessionSvc->getSession('unidad');
        $unidad = $request->get('unidadNegocio');
        $cliente = $request->get('cliente');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bCliente', $cliente);
        $this->sessionSvc->setSession('bFechaDesde', $fechaDesde);
        $this->sessionSvc->setSession('bFechaHasta', $fechaHasta);

        $em = $this->getDoctrine()->getManager();
        $tipoDocumento = 'RP';
        $entities = $em->getRepository('JOYASJoyasBundle:Documento')->filtroBusqueda($unidad, $fechaDesde, $fechaHasta, $cliente, $unidadActual, $tipoDocumento);

        if (isset($unidadActual) && $unidadActual != '') {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($this->sessionSvc->getSession('unidad'));
        } else {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
        }

        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Documento:pagos.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
                    'clientesProveedores' => $clientesProveedores,
        ));
    }

    public function gastosAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $unidadActual = $this->sessionSvc->getSession('unidad');
        $unidad = $request->get('unidadNegocio');
        $cliente = $request->get('cliente');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bCliente', $cliente);
        $this->sessionSvc->setSession('bFechaDesde', $fechaDesde);
        $this->sessionSvc->setSession('bFechaHasta', $fechaHasta);

        $em = $this->getDoctrine()->getManager();
        $tipoDocumento = 'G';
        $entities = $em->getRepository('JOYASJoyasBundle:Documento')->filtroBusqueda($unidad, $fechaDesde, $fechaHasta, $cliente, $unidadActual, $tipoDocumento);

        if (isset($unidadActual) && $unidadActual != '') {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($this->sessionSvc->getSession('unidad'));
        } else {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
        }
        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Documento:gastos.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
                    'clientesProveedores' => $clientesProveedores,
        ));
    }

    /**
     * Creates a new Documento entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = new Documento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        $entity->setFechaRegistracion(new \DateTime());

        if ($form->isValid()) {
            $movimintocc = new MovimientoCC();

            if ($entity->getTipoDocumento() != 'G') {
                $entity->setTipoGasto(NULL);
            }

            if ($entity->getTipoDocumento() != 'R' and $entity->getTipoDocumento() != 'RP' and $entity->getTipoDocumento() != 'C' && $entity->getTipoDocumento() != 'D' && $entity->getTipoDocumento() != 'RP' && $entity->getTipoDocumento() != 'A') {
                $fecha = new \DateTime($request->get('fechaDocumento'));
            } else {
                $fecha = new \DateTime();
            }

            if ($entity->getTipoDocumento() != 'G' && $entity->getTipoDocumento() != 'A' && $entity->getTipoDocumento() != 'D' && $entity->getTipoDocumento() != 'C') {
                $idClienteProveedor = $request->get('clienteProveedor');
                if (isset($idClienteProveedor) && $idClienteProveedor != '') {
                    $cliPro = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($idClienteProveedor);
                    $movimintocc->setClienteProveedor($cliPro);
                }
            }

            if ($entity->getTipoDocumento() == 'D' or $entity->getTipoDocumento() == 'C') {
                $cliPro = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($this->sessionSvc->getSession('idCliProv'));
                $movimintocc->setClienteProveedor($cliPro);
            }
            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionSvc->getSession('unidad'));
            } else {
                if ($entity->getTipoDocumento() != 'G' and $entity->getTipoDocumento() != 'A' && isset($cliPro)) {
                    $unidad = $cliPro->getUnidadNegocio();
                } else {
                    $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
                }
            }
            $movimintocc->setUnidadNegocio($unidad);

            if ($entity->getTipoDocumento() == 'R') {
                $movimintocc->setTarjeta($request->get('tarjeta'));
            }
            if ($entity->getTipoDocumento() == 'A') {
                $entity->setEfectivo($entity->getImporte());
                $entity->setFecha($fecha);
            }

            if ($entity->getTipoDocumento() == 'R') {
                $entity->setEfectivo($request->get('efectivo'));
                $entity->setFecha(new \DateTime($request->get('fechaDocumento')));
                if ($request->get('auto') != 'true') {
                    $entity->setFecha(new \DateTime($request->get('fechaDocumento')));
                } else {
                    $nrorecibo = $em->getRepository('JOYASJoyasBundle:NumeracionRecibo')->findOneBy(array('unidadNegocio' => $unidad->getId()));
                    if (is_null($nrorecibo)) {
                        $nrorecibo = new NumeracionRecibo();
                        $nrorecibo->setUnidadNegocio($unidad);
                        $nrorecibo->setNrorecibo(1);
                        $em->persist($nrorecibo);
                        $nrorec = $nrorecibo->getNrorecibo();
                    } else {
                        $nrorec = $nrorecibo->getNrorecibo() + 1;
                    }
                    $entity->setNrocomprobante('000' . $nrorec);
                }

                for ($i = 0; $i < 6; $i++) {
                    if ($request->get('banco' . $i) != 0 and $request->get('tipocheque' . $i)
                            and $request->get('importe' . $i) != '' and $request->get('importe' . $i) > 0) {

                        $banco = $em->getRepository('JOYASJoyasBundle:Banco')->find($request->get('banco' . $i));
                        $tipocheque = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($request->get('tipocheque' . $i));

                        $cheque = new Cheque();
                        $cheque->setImporte($request->get('importe' . $i));
                        $cheque->setFirmantes($request->get('firmantes' . $i));
                        $cheque->setCuit($request->get('cuit' . $i));
                        $cheque->setFechacobro(new \DateTime($request->get('fechacobro' . $i)));
                        $cheque->setFechaemision(new \DateTime($request->get('fechaemision' . $i)));
                        $cheque->setNrocheque($request->get('nrocheque' . $i));
                        $cheque->setBanco($banco);
                        $cheque->setTipocheque($tipocheque);
                        $cheque->setDocumento($entity);
                        $cheque->setUnidadNegocio($unidad);
                        $em->persist($cheque);
                        $em->flush();
                    }
                }
            }
            if ($entity->getTipoDocumento() == 'RP' || $entity->getTipoDocumento() == 'G') {
                $cheques = $request->get('selectcheques');
                $entity->setFecha(new \DateTime($request->get('fechaDocumento')));
                $entity->setEfectivo($request->request->get('efectivo'));
                $entity->setImporte($request->request->get('efectivo'));

                if (count($cheques) == 0 && $entity->getImporte() == 0) {
                    $this->sessionSvc->addFlash('msgError', 'Se debe ingresar algún cheque o importe.');
                    return $this->redirect('documento_new', array(
                                'tipo' => $entity->getTipoDocumento()
                    ));
                }
                if ($request->get('gasto')) {

                    $entity->setTipoDocumento('G');
                    $tipoGasto = $entity->getTipoGasto();
                    if (isset($tipoGasto) && $tipoGasto != '') {
                        $tGasto = $em->getRepository('JOYASJoyasBundle:TipoGasto')->find($tipoGasto);
                        $entity->setTipoGasto($tGasto);
                    } else {
                        $entity->setTipoGasto(NULL);
                    }
                    $em->flush();
                }

                if ($request->get('auto') != 'true') {
                    $entity->setFecha(new \DateTime($request->get('fechaDocumento')));
                } else {
                    $nrorecibo = $em->getRepository('JOYASJoyasBundle:NumeracionRecibo')->findOneBy(array('unidadNegocio' => $unidad->getId()));
                    if (is_null($nrorecibo)) {
                        $nrorecibo = new NumeracionRecibo();
                        $nrorecibo->setUnidadNegocio($unidad);
                        $nrorecibo->setNrorecibo(1);
                        $em->persist($nrorecibo);
                        $nrorec = $nrorecibo->getNrorecibo();
                    } else {
                        $nrorec = $nrorecibo->getNrorecibo() + 1;
                    }
                    $entity->setNrocomprobante('000' . $nrorec);
                }

                $this->sessionSvc->setSession('cheques', $cheques);
                for ($i = 0; $i < count($cheques); $i++) {
                    $chequeinactivo = $em->getRepository('JOYASJoyasBundle:Cheque')->find($cheques[$i]);
                    $chequeinactivo->setEstado('U');
                    $chequeinactivo->setPago($entity);
                    $em->flush();
                }
            }
            $em->persist($entity);
            $movimintocc->setDocumento($entity);
            $movimintocc->setTipoDocumento($entity->getTipoDocumento());
            $movimintocc->setMoneda($entity->getMoneda());
            $em->persist($movimintocc);
            $em->flush();
            $entity->setMovimientocc($movimintocc);
            $em->flush();

            if ($entity->getTipoDocumento() == "F") {
                $this->sessionSvc->addFlash('msgOk', 'Compra registrada exitosamente.');
                return $this->redirect($this->generateUrl('documento_compras'));
            }
            if ($entity->getTipoDocumento() == "R") {
                $this->sessionSvc->addFlash('msgOk', 'Cobranza registrada exitosamente.');
                return $this->redirect($this->generateUrl('documento_cobranzas'));
            }
            if ($entity->getTipoDocumento() == "RP") {
                $this->sessionSvc->addFlash('msgOk', 'Pago registrado exitosamente.');
                return $this->redirect($this->generateUrl('documento_pagos'));
            }
            if ($entity->getTipoDocumento() == "G") {
                $this->sessionSvc->addFlash('msgOk', 'Gasto registrado exitosamente.');
                return $this->redirect($this->generateUrl('documento_gastos'));
            }
            if ($entity->getTipoDocumento() == "A") {
                $this->sessionSvc->addFlash('msgOk', 'Ajuste registrado exitosamente.');
                return $this->redirect($this->generateUrl('movimientocc_admcaja'));
            }
            if ($entity->getTipoDocumento() == "D") {
                $this->sessionSvc->addFlash('msgOk', 'Nota de Débito registrada exitosamente.');
                return $this->redirect($this->generateUrl('clienteproveedor_cc', array('id' => $this->sessionSvc->getSession('idCliProv'))));
            }
            if ($entity->getTipoDocumento() == "C") {
                $this->sessionSvc->addFlash('msgOk', 'Nota de Crédito registrada exitosamente.');
                return $this->redirect($this->generateUrl('clienteproveedor_cc', array('id' => $this->sessionSvc->getSession('idCliProv'))));
            }
        }

        return $this->render('JOYASJoyasBundle:Documento:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'unidades' => $unidades,
        ));
    }

    /**
     * Creates a form to create a Documento entity.
     *
     * @param Documento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Documento $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new DocumentoType($entity->getTipoDocumento()), $entity, array(
            'action' => $this->generateUrl('documento_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first crear')));

        return $form;
    }

    /**
     * Displays a form to create a new Documento entity.
     *
     */
    public function newAction(Request $request, $tipo) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = new Documento();
        $entity->setTipoDocumento($tipo);
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $entity->setEfectivo($request->get('importe'));
        $entity->setOro($request->get('oro'));
        $entity->setPlata($request->get('plata'));
        $entity->setMoneda($request->get('moneda'));

        $form = $this->createCreateForm($entity);

        $idunidad = $request->get('unidad');
        $idUnidadActual = $this->sessionSvc->getSession('unidad');

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas($idUnidadActual);
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($idUnidadActual);
            $productos = $em->getRepository('JOYASJoyasBundle:Producto')->getAllActivas($idUnidadActual);
        }

        if ($tipo == 'F') {
            if ($idunidad==''){
                $idunidad=$idUnidadActual;
            }
            $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas($idunidad);
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($idunidad);
            $productos = $em->getRepository('JOYASJoyasBundle:Producto')->getAllActivas($idunidad);
            $tipoIngreso = $request->get('tipoIngreso');
            if ((isset($tipoIngreso) && $tipoIngreso != '' && $tipoIngreso == 'manual') || !isset($tipoIngreso)) {
                return $this->render('JOYASJoyasBundle:Documento:facturaproveedor.html.twig', array(
                            'entity' => $entity,
                            'unidad' => $unidad,
                            'productos' => $productos,
                            'clientesProveedores' => $cliProvs)
                );
            } else if ($tipoIngreso == 'proceso') {
                $proveedor = $request->get('proveedor');
                $unidadNegocio = $request->get('unidad');
                return $this->render('JOYASJoyasBundle:Documento:importarCompra.html.twig', array(
                            'proveedor' => $proveedor,
                            'unidadNegocio' => $unidadNegocio)
                );
            }
        } else {

            if ($tipo == 'R') {


                $cliente = $request->get('cliente');
                if (isset($cliente) && $cliente != '') {
                    $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('id' => $cliente));
                } else {
                    if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                        $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas();
                    } else {
                        $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas($idUnidadActual);
                    }
                }

                $bancos = $em->getRepository('JOYASJoyasBundle:Banco')->findBy(array('activo' => true), array('descripcion' => 'ASC'));
                $tipocheques = $em->getRepository('JOYASJoyasBundle:TipoCheque')->findBy(array('activo' => true), array('descripcion' => 'ASC'));
                return $this->render('JOYASJoyasBundle:Documento:newRC.html.twig', array(
                            'entity' => $entity,
                            'unidades' => $unidades,
                            'bancos' => $bancos,
                            'tipocheques' => $tipocheques,
                            'form' => $form->createView(),
                            'clientesProveedores' => $cliProvs)
                );
            } else {
                $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas();
                if ($tipo == 'RP' || $tipo == 'G') {
                    $cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array('estado' => 'A'), array('fechacobro' => 'ASC'));
                    return $this->render('JOYASJoyasBundle:Documento:newP.html.twig', array(
                                'entity' => $entity,
                                'unidades' => $unidades,
                                'cheques' => $cheques,
                                'form' => $form->createView(),
                                'tipo' => $tipo,
                                'clientesProveedores' => $cliProvs)
                    );
                } else {
                    return $this->render('JOYASJoyasBundle:Documento:new.html.twig', array(
                                'entity' => $entity,
                                'unidades' => $unidades,
                                'form' => $form->createView(),
                                'clientesProveedores' => $cliProvs)
                    );
                }
            }
        }
    }

    /**
     * Finds and displays a Documento entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Documento entity.');
        }        
        if ($entity->getTipoDocumento() == 'RP') {
            return $this->render('JOYASJoyasBundle:Documento:showP.html.twig', array(
                        'entity' => $entity,
            ));
        }
        if ($entity->getTipoDocumento() == 'G') {
            return $this->render('JOYASJoyasBundle:Documento:showG.html.twig', array(
                        'entity' => $entity,
            ));
        }
        if ($entity->getTipoDocumento() == 'F') {
            return $this->render('JOYASJoyasBundle:Documento:showFactProv.html.twig', array(
                        'entity' => $entity,
            ));
        }

        if ($entity->getTipoDocumento() == 'R') {
            return $this->render('JOYASJoyasBundle:Documento:showR.html.twig', array(
                        'entity' => $entity,
            ));
        }
        return $this->render('JOYASJoyasBundle:Documento:show.html.twig', array(
                    'entity' => $entity,
        ));
    }

    public function imprimirAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Documento entity.');
        }

        $pdfGenerator = $this->get('siphoc.pdf.generator');
        $pdfGenerator->setName('RECIBO CLIENTE ' . $entity->getId() . '.pdf');
        return $pdfGenerator->downloadFromView(
                        'JOYASJoyasBundle:Documento:imprimir.html.twig', array(
                    'entity' => $entity,)
        );
    }

    /**
     * Displays a form to edit an existing Documento entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Documento entity.');
        }

        $editForm = $this->createEditForm($entity);

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas();
        }

        return $this->render('JOYASJoyasBundle:Documento:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'clientesProveedores' => $cliProvs
        ));
    }

    /**
     * Creates a form to edit a Documento entity.
     *
     * @param Documento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Documento $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new DocumentoType($entity->getTipoDocumento()), $entity, array(
            'action' => $this->generateUrl('documento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Documento entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Documento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($entity->getTipoDocumento() != 'G') {
                $entity->setTipoGasto(NULL);
            }

            if ($entity->getTipoDocumento() == 'R') {
                $entity->getMovimientocc()->setTarjeta($request->get('tarjeta'));
            }

            $fecha = new \DateTime($request->get('fechaDocumento'));

            if ($entity->getTipoDocumento() == 'F' and $entity->getFechaRegistracion() < $fecha) {
                $this->sessionSvc->addFlash('msgError', 'La Fecha no puede ser mayor a la de registracion.');
                return $this->render('JOYASJoyasBundle:Documento:edit.html.twig', array(
                            'entity' => $entity,
                            'edit_form' => $editForm->createView(),
                ));
            } else {
                if ($entity->getTipoDocumento() == 'F') {
                    $entity->setFecha($fecha);
                }

                $em->flush();

                if ($entity->getTipoDocumento() == "F") {
                    $this->sessionSvc->addFlash('msgOk', 'Factura modificada.');
                    return $this->redirect($this->generateUrl('documento_compras'));
                }
                if ($entity->getTipoDocumento() == "R") {
                    $this->sessionSvc->addFlash('msgOk', 'Recibo modificada.');
                    return $this->redirect($this->generateUrl('documento_cobranzas'));
                }
                if ($entity->getTipoDocumento() == "RP") {
                    $this->sessionSvc->addFlash('msgOk', 'Recibo modificado.');
                    return $this->redirect($this->generateUrl('documento_pagos'));
                }
                if ($entity->getTipoDocumento() == "G") {
                    $this->sessionSvc->addFlash('msgOk', 'Gasto modificado.');
                    return $this->redirect($this->generateUrl('documento_gastos'));
                }
            }
        }

        return $this->render('JOYASJoyasBundle:Documento:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    public function borrarAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Documento')->find($id);
        $stock = '';
        $entity->setEstado('B');
        $entity->getMovimientocc()->setEstado('B');
        if ($entity->getTipoDocumento() == 'F') {
            foreach ($entity->getProductosDocumento() as $prodDoc) {
                $stock = $prodDoc->getProducto()->getStock() - $prodDoc->getCantidad();
                if ($stock < 0) {
                    $stock = 0;
                }
                $prodDoc->getProducto()->setStock($stock);
            }
        }
        $em->flush();

        if ($entity->getTipoDocumento() == "F") {
            $this->sessionSvc->addFlash('msgOk', 'Baja satisfactoria.');
            return $this->redirect($this->generateUrl('documento_compras'));
        }
        if ($entity->getTipoDocumento() == "R") {
            $this->sessionSvc->addFlash('msgOk', 'Baja satisfactoria.');
            return $this->redirect($this->generateUrl('documento_cobranzas'));
        }
        if ($entity->getTipoDocumento() == "G") {
            $this->sessionSvc->addFlash('msgOk', 'Baja satisfactoria.');
            return $this->redirect($this->generateUrl('documento_gastos'));
        }
        if ($entity->getTipoDocumento() == "RP") {
            $this->sessionSvc->addFlash('msgOk', 'Baja satisfactoria.');
            return $this->redirect($this->generateUrl('documento_pagos'));
        }
    }

    /**
     * Deletes a Documento entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Documento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Documento entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('documento'));
    }

    /**
     * Creates a form to delete a Documento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('documento_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Borrar', 'attr' => array('class' => 'btn')))
                        ->getForm()
        ;
    }

    public function informeComprasAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        return $this->render('JOYASJoyasBundle:Documento:filtro.html.twig', array(
                    'tipo' => 'compras',
                    'unidades' => $unidades
        ));
    }

    public function informeCobranzasAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        return $this->render('JOYASJoyasBundle:Documento:filtro.html.twig', array(
                    'tipo' => 'cobranzas',
                    'unidades' => $unidades
        ));
    }

    public function informePagosAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        return $this->render('JOYASJoyasBundle:Documento:filtro.html.twig', array(
                    'tipo' => 'pagos',
                    'unidades' => $unidades
        ));
    }

    public function informeVentasAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        return $this->render('JOYASJoyasBundle:Documento:filtro.html.twig', array(
                    'tipo' => 'ventas',
                    'unidades' => $unidades
        ));
    }

    public function informeGastosAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        return $this->render('JOYASJoyasBundle:Documento:filtro.html.twig', array(
                    'tipo' => 'gastos',
                    'unidades' => $unidades
        ));
    }

    public function informesAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $desde = new \DateTime($request->get('fechaDesde'));
        $hasta = new \DateTime($request->get('fechaHasta'));
        $saldoP = 0;
        $saldoD = 0;
        $porcP = 0;
        $porcD = 0;

        $em = $this->getDoctrine()->getManager();
        // ask the service for a Excel5
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator($this->container->getParameter('nombre_cliente'))
                ->setLastModifiedBy($this->container->getParameter('nombre_cliente'))
                ->setTitle("Informe")
                ->setSubject($this->container->getParameter('nombre_cliente'));

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Fecha: ' . date('d-m-Y'))
                ->setCellValue('A2', 'Periodo')
                ->setCellValue('B2', 'Desde: ' . $desde->format('d-m-Y'))
                ->setCellValue('C2', 'Hasta: ' . $hasta->format('d-m-Y'));

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $documentos = $em->getRepository('JOYASJoyasBundle:Documento')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $documentos = $em->getRepository('JOYASJoyasBundle:Documento')->getAllActivas($request->get('unidadnegocio'));
        }

        $count = 4;
        if ($request->get('agrupado') == '1') {
            if ($request->get('tipo') == 'ventas') {

                if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                    $ventas = $em->getRepository('JOYASJoyasBundle:Factura')->getAllActivas($this->sessionSvc->getSession('unidad'));
                } else {
                    $ventas = $em->getRepository('JOYASJoyasBundle:Factura')->getAllActivas($request->get('unidadnegocio'));
                }

                $phpExcelObject->getActiveSheet()->setTitle('Informe de Ventas');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Ventas ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Ventas (u$s)');

                foreach ($ventas as $venta) {
                    if ($desde < $venta->getFecha() and $venta->getFecha() < $hasta) {
                        $importeFC = 0;
                        if ($venta->getMovimientoCC()->getMoneda() == '1') {
                            foreach ($venta->getProductosFactura() as $prodFact) {
                                $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                            }

                            if ($venta->getDescuento() != 0 and $venta->getDescuento() != '') {
                                $desc = $importeFC * ($venta->getDescuento() / 100);
                                $importeFC = $importeFC - $desc;
                            }

                            $importeFC = $importeFC - $venta->getBonificacion();
                            $saldoP = $saldoP + $importeFC;
                        } else {
                            foreach ($venta->getProductosFactura() as $prodFact) {
                                $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                            }

                            if ($venta->getDescuento() != 0 and $venta->getDescuento() != '') {
                                $desc = $importeFC * ($venta->getDescuento() / 100);
                                $importeFC = $importeFC - $desc;
                            }

                            $importeFC = $importeFC - $venta->getBonificacion();
                            $saldoD = $saldoD + $importeFC;
                        }
                    }
                }

                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($ventas as $venta) {
                    if ($desde < $venta->getFecha() and $venta->getFecha() < $hasta) {
                        $importeFC = 0;

                        foreach ($venta->getProductosFactura() as $prodFact) {
                            $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                        }

                        if ($venta->getDescuento() != 0 and $venta->getDescuento() != '') {
                            $desc = $importeFC * ($venta->getDescuento() / 100);
                            $importeFC = $importeFC - $desc;
                        }

                        $importeFC = $importeFC - $venta->getBonificacion();

                        if ($fechaAnt == '') {
                            $fechaAnt = $venta->getFecha()->format('d-m-Y');

                            if ($venta->getMovimientoCC()->getMoneda() == '1') {
                                $valorAntP = $valorAntP + $importeFC;
                                $porcP = ($valorAntP * 100) / $saldoP;
                            } else {
                                $valorAntD = $valorAntD + $importeFC;
                                $porcD = ($valorAntD * 100) / $saldoD;
                            }
                        } else {
                            if ($fechaAnt == $venta->getFecha()->format('d-m-Y')) {
                                $fechaAnt = $venta->getFecha()->format('d-m-Y');

                                if ($venta->getMovimientoCC()->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $importeFC;
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $importeFC;
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('A' . $count, $fechaAnt)
                                        ->setCellValue('B' . $count, round($porcP, 2))
                                        ->setCellValue('C' . $count, $valorAntP)
                                        ->setCellValue('D' . $count, round($porcD, 2))
                                        ->setCellValue('E' . $count, $valorAntD);
                                $valorAntD = 0;
                                $valorAntP = 0;
                                $porcP = 0;
                                $porcD = 0;
                                if ($venta->getMovimientoCC()->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $importeFC;
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $importeFC;
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                                $fechaAnt = $venta->getFecha()->format('d-m-Y');
                                $count = $count + 1;
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }

            if ($request->get('tipo') == 'compras') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Compras');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Compras ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Compras (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "F") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }
                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "F") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }

            if ($request->get('tipo') == 'cobranzas') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Cobranzas');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Cobranzas ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Cobranzas (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "R") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }

                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "R") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }

            if ($request->get('tipo') == 'pagos') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Pagos');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Pagos ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Pagos (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "RP") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }


                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "RP") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }


            if ($request->get('tipo') == 'gastos') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Gastos');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Gastos ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Gastos (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "G") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }

                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "G") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('d-m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('d-m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }
        } else {
            // AGRUPAMIENTO MES

            if ($request->get('tipo') == 'ventas') {

                $ventas = $em->getRepository('JOYASJoyasBundle:Factura')->findAll();

                $phpExcelObject->getActiveSheet()->setTitle('Informe de Ventas');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Ventas ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Ventas (u$s)');

                foreach ($ventas as $venta) {
                    if ($desde < $venta->getFecha() and $venta->getFecha() < $hasta) {
                        $importeFC = 0;
                        if ($venta->getMovimientoCC()->getMoneda() == '1') {
                            foreach ($venta->getProductosFactura() as $prodFact) {
                                $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                            }

                            if ($venta->getDescuento() != 0 and $venta->getDescuento() != '') {
                                $desc = $importeFC * ($venta->getDescuento() / 100);
                                $importeFC = $importeFC - $desc;
                            }

                            $importeFC = $importeFC - $venta->getBonificacion();
                            $saldoP = $saldoP + $importeFC;
                        } else {
                            foreach ($venta->getProductosFactura() as $prodFact) {
                                $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                            }

                            if ($venta->getDescuento() != 0 and $venta->getDescuento() != '') {
                                $desc = $importeFC * ($venta->getDescuento() / 100);
                                $importeFC = $importeFC - $desc;
                            }

                            $importeFC = $importeFC - $venta->getBonificacion();
                            $saldoD = $saldoD + $importeFC;
                        }
                    }
                }

                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($ventas as $venta) {
                    if ($desde < $venta->getFecha() and $venta->getFecha() < $hasta) {
                        $importeFC = 0;

                        foreach ($venta->getProductosFactura() as $prodFact) {
                            $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                        }

                        if ($venta->getDescuento() != 0 and $venta->getDescuento() != '') {
                            $desc = $importeFC * ($venta->getDescuento() / 100);
                            $importeFC = $importeFC - $desc;
                        }

                        $importeFC = $importeFC - $venta->getBonificacion();

                        if ($fechaAnt == '') {
                            $fechaAnt = $venta->getFecha()->format('m-Y');

                            if ($venta->getMovimientoCC()->getMoneda() == '1') {
                                $valorAntP = $valorAntP + $importeFC;
                                $porcP = ($valorAntP * 100) / $saldoP;
                            } else {
                                $valorAntD = $valorAntD + $importeFC;
                                $porcD = ($valorAntD * 100) / $saldoD;
                            }
                        } else {
                            if ($fechaAnt == $venta->getFecha()->format('m-Y')) {
                                $fechaAnt = $venta->getFecha()->format('m-Y');

                                if ($venta->getMovimientoCC()->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $importeFC;
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $importeFC;
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('A' . $count, $fechaAnt)
                                        ->setCellValue('B' . $count, round($porcP, 2))
                                        ->setCellValue('C' . $count, $valorAntP)
                                        ->setCellValue('D' . $count, round($porcD, 2))
                                        ->setCellValue('E' . $count, $valorAntD);
                                $valorAntD = 0;
                                $valorAntP = 0;
                                $porcP = 0;
                                $porcD = 0;
                                if ($venta->getMovimientoCC()->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $importeFC;
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $importeFC;
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                                $fechaAnt = $venta->getFecha()->format('m-Y');
                                $count = $count + 1;
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }

            if ($request->get('tipo') == 'compras') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Compras');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Compras ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Compras (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "F") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }
                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    $this->sessionSvc->setSession('MES', $doc->getFechaRegistracion()->format('m-Y'));
                    if ($doc->getTipoDocumento() == "F") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }

            if ($request->get('tipo') == 'cobranzas') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Cobranzas');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Cobranzas ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Cobranzas (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "R") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }

                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "R") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }

            if ($request->get('tipo') == 'pagos') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Pagos');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Pagos ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Pagos (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "RP") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }


                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "RP") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }


            if ($request->get('tipo') == 'gastos') {
                $phpExcelObject->getActiveSheet()->setTitle('Informe de Gastos');
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A3', 'Dia/Mes')
                        ->setCellValue('B3', '% del total del periodo')
                        ->setCellValue('C3', 'Gastos ($)')
                        ->setCellValue('D3', '% del total del periodo')
                        ->setCellValue('E3', 'Gastos (u$s)');
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "G") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($doc->getMoneda() == '1') {
                                $saldoP = $saldoP + $doc->getImporte();
                            } else {
                                $saldoD = $saldoD + $doc->getImporte();
                            }
                        }
                    }
                }

                $fechaAnt = '';
                $valorAntP = 0;
                $valorAntD = 0;
                foreach ($documentos as $doc) {
                    if ($doc->getTipoDocumento() == "G") {
                        if ($desde < $doc->getFechaRegistracion() and $doc->getFechaRegistracion() < $hasta) {
                            if ($fechaAnt == '') {
                                $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                if ($doc->getMoneda() == '1') {
                                    $valorAntP = $valorAntP + $doc->getImporte();
                                    $porcP = ($valorAntP * 100) / $saldoP;
                                } else {
                                    $valorAntD = $valorAntD + $doc->getImporte();
                                    $porcD = ($valorAntD * 100) / $saldoD;
                                }
                            } else {
                                if ($fechaAnt == $doc->getFechaRegistracion()->format('m-Y')) {
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                } else {
                                    $phpExcelObject->setActiveSheetIndex(0)
                                            ->setCellValue('A' . $count, $fechaAnt)
                                            ->setCellValue('B' . $count, round($porcP, 2))
                                            ->setCellValue('C' . $count, $valorAntP)
                                            ->setCellValue('D' . $count, round($porcD, 2))
                                            ->setCellValue('E' . $count, $valorAntD);
                                    $valorAntD = 0;
                                    $valorAntP = 0;
                                    $porcP = 0;
                                    $porcD = 0;
                                    if ($doc->getMoneda() == '1') {
                                        $valorAntP = $valorAntP + $doc->getImporte();
                                        $porcP = ($valorAntP * 100) / $saldoP;
                                    } else {
                                        $valorAntD = $valorAntD + $doc->getImporte();
                                        $porcD = ($valorAntD * 100) / $saldoD;
                                    }
                                    $fechaAnt = $doc->getFechaRegistracion()->format('m-Y');
                                    $count = $count + 1;
                                }
                            }
                        }
                    }
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $fechaAnt)
                        ->setCellValue('B' . $count, round($porcP, 2))
                        ->setCellValue('C' . $count, $valorAntP)
                        ->setCellValue('D' . $count, round($porcD, 2))
                        ->setCellValue('E' . $count, $valorAntD);
                $count = $count + 1;
            }
        }

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A' . $count, 'TOTALES')
                ->setCellValue('B' . $count, '100%')
                ->setCellValue('C' . $count, '$' . $saldoP)
                ->setCellValue('D' . $count, '100%')
                ->setCellValue('E' . $count, 'u$s' . $saldoD);

        $phpExcelObject->getActiveSheet()->getStyle('A3:E3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->getStyle('A' . $count . ':E' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');

        if ($request->get('tipo') == 'compras') {
            $response->headers->set('Content-Disposition', 'attachment;filename=InformeCompras.xls');
        }

        if ($request->get('tipo') == 'cobranzas') {
            $response->headers->set('Content-Disposition', 'attachment;filename=InformeCobranzas.xls');
        }

        if ($request->get('tipo') == 'pagos') {
            $response->headers->set('Content-Disposition', 'attachment;filename=InformePagos.xls');
        }

        if ($request->get('tipo') == 'ventas') {
            $response->headers->set('Content-Disposition', 'attachment;filename=InformeVentas.xls');
        }


        if ($request->get('tipo') == 'gastos') {
            $response->headers->set('Content-Disposition', 'attachment;filename=InformeGastos.xls');
        }

        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    public function facturaproveedorAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = new Documento();
        $movimientocc = new MovimientoCC();
        $entity->setTipoGasto(NULL);
        $valor = 0;
        $fecha = new \DateTime($request->get('fechaDocumento'));
        $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidad'));
        $cliPro = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($request->get('proveedor'));

        $movimientocc->setUnidadNegocio($unidad);
        $movimientocc->setClienteProveedor($cliPro);
        $movimientocc->setDocumento($entity);
        $movimientocc->setTipoDocumento('F');
        $movimientocc->setMoneda($request->get('moneda'));

        $entity->setMoneda($request->get('moneda'));
        $entity->setDescuento($request->get('descuento'));
        $entity->setFecha($fecha);
        $entity->setBonificacion($request->get('bonificacion'));
        $entity->setTipoDocumento('F');
        $entity->setOro($request->get('oro'));
        $entity->setPlata($request->get('plata'));

        $entity->setImporte($valor);

        $em->persist($entity);
        $em->flush();
        $em->persist($movimientocc);
        $em->flush();

        $entity->setMovimientocc($movimientocc);

        $contador = $request->get('contador');

        for ($x = 0; $x <= $contador; $x++) {
            $codigo = $request->get('codigo' . $x);
            $desc = $request->get('descripcion' . $x);
            $precio = $request->get('precio' . $x);
            $cantidad = $request->get('cantidad' . $x);
            $oro = $request->get('oro' . $x);
            $plata = $request->get('plata' . $x);

            if (!is_null($codigo) and $codigo != '' and $desc != '' and ! is_null($desc) and $precio != '' and ! is_null($precio)and $cantidad != '' and ! is_null($cantidad)) {

                $productoDocumento = new ProductoDocumento();

                if ($codigo == '000') {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo));
                    $productoDocumento->setProducto($producto);
                    $productoDocumento->setDocumento($entity);
                    $productoDocumento->setPrecio($request->get('precio' . $x));
                    $productoDocumento->setCantidad($request->get('cantidad' . $x));
                    $entity->addProductosDocumento($productoDocumento);
                } else {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio' => $unidad));

                    if (is_null($producto)) {
                        $producto = new Producto();
                        $producto->setCodigo($codigo);
                        $producto->setDescripcion($desc);
                        $producto->setUnidadNegocio($unidad);
                        $producto->setMoneda($request->get('moneda'));
                        $producto->setStock($request->get('cantidad' . $x));
                        $em->persist($producto);
                        $producto->setCosto($request->get('precio' . $x));
                    } else {
                        $stock = $producto->getStock() + $request->get('cantidad' . $x);
                        $producto->setStock($stock);
                        if ($producto->getMoneda() == $request->get('moneda')) {
                            $producto->setCosto($request->get('precio' . $x));
                        }
                    }
                    $oro = $request->get('oro' . $x);
                    if (!isset($oro) || empty($oro)) {
                        $producto->setOro(0);
                    } else {
                        $producto->setOro($request->get('oro' . $x));
                    }
                    $plata = $request->get('plata' . $x);
                    if (!isset($plata) || empty($plata)) {
                        $producto->setPlata(0);
                    } else {
                        $producto->setPlata($request->get('plata' . $x));
                    }

                    $productoDocumento->setProducto($producto);
                    $productoDocumento->setDocumento($entity);
                    $productoDocumento->setPrecio($request->get('precio' . $x));
                    $productoDocumento->setCantidad($request->get('cantidad' . $x));

                    $entity->addProductosDocumento($productoDocumento);
                    $em->persist($productoDocumento);
                }
                $valor = $valor + ($precio * $cantidad);
                $em->flush();
            }
        }

        if (count($entity->getProductosDocumento()) == 0) {
            $em->remove($movimientocc);
            $em->flush();
            $em->remove($entity);
            $em->flush();
            $this->sessionSvc->addFlash('msgError', 'No puede generar una factura sin productos.');
            return $this->redirect($this->generateUrl('documento_compras'));
        }

        $entity->setImporte($valor);
        $em->flush();

        $this->sessionSvc->addFlash('msgOk', 'Compra registrada.');
        return $this->redirect($this->generateUrl('documento_show', array('id' => $entity->getId())));
    }

    public function importarCompraAction(Request $request, $proveedor, $unidadNegocio) {
        $em = $this->getDoctrine()->getManager();
        $uploaddir = $this->get('kernel')->getRootDir() . "/../web/uploads/documents/";
        // Cell caching ::
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
        $filename = '';
        $a = $request->get('desde');
        $b = $a + 100;

        if ($a == 1) {
            $filename = trim($_FILES['archivo']['name']);

            $uploadfile = $uploaddir . $filename;
            if (!move_uploaded_file($_FILES['archivo']['tmp_name'], $uploadfile)) {
                $this->sessionSvc->addFlash('msgError', 'No se pudo cargar el archivo.');
                return $this->render('JOYASJoyasBundle:Documento:importarCompra.html.twig', array(
                            'proveedor' => $proveedor,
                            'unidadNegocio' => $unidadNegocio));
            } else {
                ini_set('memory_limit', '-1');
                $inputFileType = 'Excel5';
                $sheetname = 'Hoja1';
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objReader->setLoadSheetsOnly($sheetname);
                $objPHPExcel = $objReader->load($uploadfile);

                $documento = new Documento();
                $documento->setNrocomprobante($objPHPExcel->getActiveSheet()->getCell('B2')->getValue());
                $fecha = $objPHPExcel->getActiveSheet()->getCell('B1')->getCalculatedValue();
                if (is_numeric($fecha)) {
                    $timestamp = \PHPExcel_Shared_Date::ExcelToPHP($fecha);
                    $fecha = gmdate("d-m-Y", $timestamp);
                }
                $documento->setFecha(new \DateTime(str_replace('/', '-', $fecha)));
                $documento->setFechaRegistracion(new \DateTime());
                $documento->setMoneda(2);
                $documento->setEstado('A');
                $documento->setTipoDocumento('F');
                $documento->setImporte(0);
                $documento->setPlata(0);
                $documento->setOro(0);
                $this->sessionSvc->setSession('total', 0);
                $documento->setDescuento(0);
                $documento->setBonificacion(0);
                $em->persist($documento);
                $em->flush();

                $movimientocc = new MovimientoCC();
                $movimientocc->setDocumento($documento);
                $movimientocc->setEstado('A');

                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($unidadNegocio);
                $movimientocc->setUnidadNegocio($unidad);
                $movimientocc->setTipoDocumento('F');
                $movimientocc->setFechaRegistracion(new \DateTime());
                $movimientocc->setMoneda(2);
                $prov = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($proveedor);
                $movimientocc->setClienteProveedor($prov);

                $em->persist($movimientocc);
                $em->flush();
                $documento->setMovimientocc($movimientocc);
                $em->persist($documento);
                $em->flush();
                $this->sessionSvc->setSession('documentoId', $documento->getId());
            }
        } else {
            $uploadfile = $this->sessionSvc->getSession('uploadfile');
            ini_set('memory_limit', '-1');
            $inputFileType = 'Excel5';
            $sheetname = 'Hoja1';
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setLoadSheetsOnly($sheetname);
            $objPHPExcel = $objReader->load($uploadfile);
            $documento = $em->getRepository('JOYASJoyasBundle:Documento')->find($this->sessionSvc->getSession('documentoId'));
        }

        if (!file_exists($uploadfile)) {
            $this->sessionSvc->addFlash('msgError', 'No se pudo migrar el archivo.');
            return $this->render('JOYASJoyasBundle:Documento:importarCompra.html.twig', array(
                        'proveedor' => $proveedor,
                        'unidadNegocio' => $unidadNegocio));
        }

        $vacio = false;
        $tipoImportacion = $this->validarImportacion($objPHPExcel->getActiveSheet());

        if (!$tipoImportacion) {
            $this->sessionSvc->addFlash('msgError', 'El formato de las columnas ingresadas no corresponde con un formacto válido.');
            return $this->render('JOYASJoyasBundle:Documento:importarCompra.html.twig', array(
                        'proveedor' => $proveedor,
                        'unidadNegocio' => $unidadNegocio));
        }
        for ($i = $a; $i <= $b; $i++) {
            if (!$vacio && $i > 4) {
                switch ($tipoImportacion) {
                    case 'ACERO':
                        $this->importarAcero($objPHPExcel->getActiveSheet(), $i, $documento);
                        break;
                    case 'PLATAHR':
                        $this->importarPlataHr($objPHPExcel->getActiveSheet(), $i, $documento);
                        break;
                    case 'PLATA':
                        $this->importarPlata($objPHPExcel->getActiveSheet(), $i, $documento);
                        break;
                }

                $j = $i + 1;
                $control = $objPHPExcel->getActiveSheet()->getCell('B' . $j)->getValue();
                if (!isset($control)) {
                    $vacio = true;
                }
            }
            $cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
            $cacheDriver->deleteAll();
        }

        // ENVIAR DATOS
        if (!$vacio) {
            $this->sessionSvc->setSession('desde', $i);
            $this->sessionSvc->setSession('origen', 'importarCompra');
            $this->sessionSvc->setSession('fin', 'no');
            $this->sessionSvc->setSession('uploadfile', $uploadfile);
            //$this->sessionSvc->setSession('unidad', $unidad->getId());
            $this->sessionSvc->addFlash('msgWarn', 'Se importado desde la línea ' . $a . ' hasta la línea ' . $i . '. Y continuando...');
            return $this->redirect($this->generateUrl('joyas_joyas_inicio'));
        } else {
            $documento->setImporte(round($this->sessionSvc->getSession('total'), 2));
            $em->persist($documento);
            $em->flush();
            $this->sessionSvc->addFlash('msgOk', 'Carga satisfactoria. Última línea: ' . $i);
            $this->sessionSvc->setSession('desde', '');
            $this->sessionSvc->setSession('origen', '');
            $this->sessionSvc->setSession('fin', '');
            $this->sessionSvc->setSession('uploadfile', '');
            $this->sessionSvc->setSession('documentoId', '');
            $this->sessionSvc->setSession('total', '');
        }
        return $this->redirect($this->generateUrl('documento_compras'));
    }

    private function importarAcero($objExcel, $i, $documento) {
        $em = $this->getDoctrine()->getManager();

        $reference = $objExcel->getCell('A' . $i)->getValue();
        $codigo = $objExcel->getCell('B' . $i)->getValue();
        //$descripcionIngles = $objExcel->getCell('C' . $i)->getValue();
        $descripcionEspanol = $objExcel->getCell('D' . $i)->getValue();
        $component = $objExcel->getCell('E' . $i)->getValue();
        $qty = $objExcel->getCell('F' . $i)->getCalculatedValue();
        $unit = $objExcel->getCell('G' . $i)->getValue();
        $unitPrice = floatval($objExcel->getCell('H' . $i)->getCalculatedValue());

        //Busco si existe el producto por la referencia
        //del proveedor
        $producto = $em->getRepository('JOYASJoyasBundle:Producto')->getByReference($reference, $documento->getMovimientocc()->getUnidadNegocio()->getId());

        if (!isset($producto)) {
            $producto = new Producto();
            $ultimoProducto = $em->getRepository('JOYASJoyasBundle:Producto')->getUltimoProducto($codigo, $documento->getMovimientocc()->getUnidadNegocio()->getId());
            if (isset($ultimoProducto)) {
                $incrementador = intval(substr($ultimoProducto->getCodigo(), -4));
                $incrementador++;
                $codigo = $codigo . str_pad($incrementador, 4, "0", STR_PAD_LEFT);
            } else {
                $codigo = $codigo . '0001';
            }
            $producto->setCodigo($codigo);
            $producto->setDescripcion($descripcionEspanol);
            $producto->setMoneda(2);
            $producto->setOro(0);
            $producto->setPlata(0);
            $producto->setQuilate(0);
            $producto->setUnidadNegocio($documento->getMovimientocc()->getUnidadNegocio());
            $producto->setReferencia($reference);
            $producto->setStock($qty);
        } else {
            $producto->setStock($producto->getStock() + $qty);
        }
        $producto->setCosto($unitPrice);
        $em->persist($producto);
        $em->flush();

        $productoDocumento = new ProductoDocumento();
        $productoDocumento->setDocumento($documento);
        $productoDocumento->setEstado('A');
        $productoDocumento->setProducto($producto);
        $productoDocumento->setPrecio($unitPrice);
        $productoDocumento->setCantidad($qty);

        $em->persist($productoDocumento);
        $em->flush();
        $this->sessionSvc->setSession('total', $this->sessionSvc->getSession('total') + $productoDocumento->getPrecio() * $productoDocumento->getCantidad());

        return true;
        //Aca generar el documento de compra con el proveedor que corresponda
        //if (error) {
        //    $this->sessionSvc->addFlash('msgError', 'La línea ' . $i . ' no tiene id de categoria-subcategoria');
        //    break;
        //}
    }

    private function importarPlataHr($objExcel, $i, $documento) {
        $em = $this->getDoctrine()->getManager();

        //$referencia = $objExcel->getCell('A' . $i)->getValue();
        $referenciaSiesa = $objExcel->getCell('B' . $i)->getValue();
        //$unidadMedida = $objExcel->getCell('C' . $i)->getValue();
        $descripcion = $objExcel->getCell('D' . $i)->getValue();
        $nroPares = $objExcel->getCell('E' . $i)->getValue();
        $unidades = $objExcel->getCell('F' . $i)->getCalculatedValue();
        $peso = $objExcel->getCell('G' . $i)->getValue();
        $precioGramo = floatval($objExcel->getCell('H' . $i)->getCalculatedValue());
        $granTotal = floatval($objExcel->getCell('I' . $i)->getCalculatedValue());
        //$plata = $objExcel->getCell('J' . $i)->getCalculatedValue();
        //$quilate = floatval($objExcel->getCell('K' . $i)->getCalculatedValue());
        $arete = $objExcel->getCell('L' . $i)->getCalculatedValue();
        $dije = $objExcel->getCell('M' . $i)->getCalculatedValue();
        $aro = $objExcel->getCell('N' . $i)->getCalculatedValue();
        $cadena = $objExcel->getCell('O' . $i)->getCalculatedValue();

        if ($arete == 'SI' || $aro == 'SI') {
            $cantidad = $nroPares;
        } else {
            $cantidad = $unidades;
        }

        //Busco si existe el producto por código (referenciaSiesa
        //del proveedor
        $producto = $em->getRepository('JOYASJoyasBundle:Producto')->getByCodigo($referenciaSiesa, $documento->getMovimientocc()->getUnidadNegocio()->getId());

        if (!isset($producto)) {
            $producto = new Producto();
            $producto->setCodigo($referenciaSiesa);
            $producto->setDescripcion($descripcion);
            $producto->setMoneda(2);
            $producto->setOro(0);
            $producto->setPlata(0);
            $producto->setQuilate(0);
            $categoriaSubcategoriaId = $this->getCategoriaSubcategoriaId($arete, $dije, $aro, $cadena);
            $categoriaSubcategoria = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->find($categoriaSubcategoriaId);
            $producto->setCategoriasubcategoria($categoriaSubcategoria);
            $producto->setUnidadNegocio($documento->getMovimientocc()->getUnidadNegocio());
            $producto->setStock($cantidad);
        } else {
            $producto->setStock($producto->getStock() + $cantidad);
        }
        if ($cantidad > 0) {
            $producto->setCosto(round($granTotal / $cantidad, 2));
        } else {
            $producto->setCosto(0);
        }
        $em->persist($producto);
        $em->flush();

        $productoDocumento = new ProductoDocumento();
        $productoDocumento->setDocumento($documento);
        $productoDocumento->setEstado('A');
        $productoDocumento->setProducto($producto);
        $productoDocumento->setPrecio($precioGramo);
        $productoDocumento->setCantidad($peso);

        $em->persist($productoDocumento);
        $em->flush();
        $this->sessionSvc->setSession('total', $this->sessionSvc->getSession('total') + $productoDocumento->getPrecio() * $productoDocumento->getCantidad());

        return true;
        //Aca generar el documento de compra con el proveedor que corresponda
        //if (error) {
        //    $this->sessionSvc->addFlash('msgError', 'La línea ' . $i . ' no tiene id de categoria-subcategoria');
        //    break;
        //}
    }

    private function importarPlata($objExcel, $i, $documento) {
        $em = $this->getDoctrine()->getManager();

        $reference = $objExcel->getCell('A' . $i)->getValue();
        $codigo = $objExcel->getCell('B' . $i)->getValue();
        //$descripcionIngles = $objExcel->getCell('C' . $i)->getValue();
        $descripcionEspanol = $objExcel->getCell('D' . $i)->getValue();
        $component = $objExcel->getCell('E' . $i)->getValue();
        //$brand = $objExcel->getCell('F' . $i)->getValue();
        $qty = $objExcel->getCell('G' . $i)->getCalculatedValue();
        //$peso = $objExcel->getCell('H' . $i)->getCalculatedValue();       
        //$unit = $objExcel->getCell('I' . $i)->getValue();
        $unitPrice = floatval($objExcel->getCell('J' . $i)->getCalculatedValue());
        $totalAmount = floatval($objExcel->getCell('K' . $i)->getCalculatedValue());

        //Busco si existe el producto por la referencia
        //del proveedor
        $producto = $em->getRepository('JOYASJoyasBundle:Producto')->getByReference($reference, $documento->getMovimientocc()->getUnidadNegocio()->getId());

        if (!isset($producto)) {
            $producto = new Producto();
            $ultimoProducto = $em->getRepository('JOYASJoyasBundle:Producto')->getUltimoProducto($codigo, $documento->getMovimientocc()->getUnidadNegocio()->getId());
            if (isset($ultimoProducto)) {
                $incrementador = intval(substr($ultimoProducto->getCodigo(), -4));
                $incrementador++;
                $codigo = $codigo . str_pad($incrementador, 4, "0", STR_PAD_LEFT);
            } else {
                $codigo = $codigo . '0001';
            }
            $producto->setCodigo($codigo);
            $producto->setDescripcion($descripcionEspanol);
            $producto->setMoneda(2);
            $producto->setOro(0);
            $producto->setPlata(0);
            $producto->setQuilate(0);
            $producto->setUnidadNegocio($documento->getMovimientocc()->getUnidadNegocio());
            $producto->setReferencia($reference);
            $producto->setStock($qty);
        } else {
            $producto->setStock($producto->getStock() + $qty);
        }
        $producto->setCosto($unitPrice);
        $em->persist($producto);
        $em->flush();

        $productoDocumento = new ProductoDocumento();
        $productoDocumento->setDocumento($documento);
        $productoDocumento->setEstado('A');
        $productoDocumento->setProducto($producto);
        $productoDocumento->setPrecio($unitPrice);
        $productoDocumento->setCantidad($qty);

        $em->persist($productoDocumento);
        $em->flush();
        $this->sessionSvc->setSession('total', $this->sessionSvc->getSession('total') + $productoDocumento->getPrecio() * $productoDocumento->getCantidad());

        return true;
        //Aca generar el documento de compra con el proveedor que corresponda
        //if (error) {
        //    $this->sessionSvc->addFlash('msgError', 'La línea ' . $i . ' no tiene id de categoria-subcategoria');
        //    break;
        //}
    }

    private function getCategoriaSubcategoriaId($arete, $dije, $aro, $cadena) {
        if ($arete == 'SI' || $aro == 'SI') {
            return 49;
        }
        if ($dije == 'SI') {
            return 42;
        }
        if ($cadena == 'SI') {
            return 45;
        }
        return null;
    }

    private function validarImportacion($objExcel) {
        if (trim($objExcel->getCell('A4')->getValue()) == 'REFERENCE' &&
                trim($objExcel->getCell('B4')->getValue()) == 'CODIGO' &&
                trim($objExcel->getCell('C4')->getValue()) == 'DESCRIPTION IN ENGLISH' &&
                trim($objExcel->getCell('D4')->getValue()) == 'DESCRIPTION EN ESPANOL' &&
                trim($objExcel->getCell('E4')->getValue()) == 'COMPONENT' &&
                trim($objExcel->getCell('F4')->getValue()) == 'QTY' &&
                trim($objExcel->getCell('G4')->getValue()) == 'UNIT' &&
                trim($objExcel->getCell('H4')->getValue()) == 'UNIT PRICE' &&
                trim($objExcel->getCell('I4')->getValue()) == 'TOTAL PRICE') {
            return 'ACERO';
        } else if (trim($objExcel->getCell('A4')->getValue()) == 'REFERENCIA' &&
                trim($objExcel->getCell('B4')->getValue()) == 'REFERENCIA SIESA' &&
                trim($objExcel->getCell('C4')->getValue()) == 'UND MDDA' &&
                trim($objExcel->getCell('D4')->getValue()) == 'DESCRIPCION' &&
                trim($objExcel->getCell('E4')->getValue()) == 'N° DE PARES' &&
                trim($objExcel->getCell('F4')->getValue()) == 'UNIDADES' &&
                trim($objExcel->getCell('G4')->getValue()) == 'P.NETO' &&
                trim($objExcel->getCell('H4')->getValue()) == 'PRECIO POR GRAMO USD' &&
                trim($objExcel->getCell('I4')->getValue()) == 'GRAN TOTAL USD' &&
                trim($objExcel->getCell('J4')->getValue()) == 'PLATA' &&
                trim($objExcel->getCell('K4')->getValue()) == 'QUILATE' &&
                trim($objExcel->getCell('L4')->getValue()) == 'ARETE' &&
                trim($objExcel->getCell('M4')->getValue()) == 'ANILLO' &&
                trim($objExcel->getCell('N4')->getValue()) == 'DIJE' &&
                trim($objExcel->getCell('O4')->getValue()) == 'ARO' &&
                trim($objExcel->getCell('P4')->getValue()) == 'CADENA'
        ) {
            return 'PLATAHR';
        } else if (trim($objExcel->getCell('A4')->getValue()) == 'REFERENCE' &&
                trim($objExcel->getCell('B4')->getValue()) == 'CODIGO' &&
                trim($objExcel->getCell('C4')->getValue()) == 'DESCRIPTION IN ENGLISH' &&
                trim($objExcel->getCell('D4')->getValue()) == 'DESCRIPTION EN ESPANOL' &&
                trim($objExcel->getCell('E4')->getValue()) == 'COMPONENT' &&
                trim($objExcel->getCell('F4')->getValue()) == 'BRAND' &&
                trim($objExcel->getCell('G4')->getValue()) == 'QTY' &&
                trim($objExcel->getCell('H4')->getValue()) == 'WEIGHT' &&
                trim($objExcel->getCell('I4')->getValue()) == 'UNIT' &&
                trim($objExcel->getCell('J4')->getValue()) == 'UNIT PRICE' &&
                trim($objExcel->getCell('K4')->getValue()) == 'TOTAL AMOUNT'
        ) {
            return 'PLATA';
        } else {
            return false;
        }
    }

    private function getLetra($columna) {
        switch ($columna) {
            case 1:
                return 'A';
            case 2:
                return 'B';
            case 3:
                return 'C';
            case 4:
                return 'D';
            case 5:
                return 'E';
            case 6:
                return 'F';
            case 7:
                return 'G';
            case 8:
                return 'H';
            case 9:
                return 'I';
            case 10:
                return 'J';
            case 11:
                return 'K';
            case 12:
                return 'L';
            case 13:
                return 'M';
            case 14:
                return 'N';
        }
    }

}
