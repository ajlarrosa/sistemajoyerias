<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\Precio;
use JOYAS\JoyasBundle\Form\PrecioType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Precio controller.
 *
 */
class PrecioController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Precio entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $idProducto = $request->get('idProducto');
        $unidadActual = $this->sessionSvc->getSession('unidad');        
        $unidad = $request->get('unidadNegocio');
        $listaPrecio = $request->get('listaPrecio');        
        $busqueda = $request->get('busqueda');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bBusqueda', $busqueda);
        $this->sessionSvc->setSession('bListaPrecio', $listaPrecio);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Precio')->filtroBusqueda($unidad, $listaPrecio, $busqueda, $idProducto, $unidadActual);
        if (isset($idProducto) && $idProducto != '') {
            if (count($entities) < 1) {
                $producto = $em->getRepository('JOYASJoyasBundle:Producto')->find($idProducto);
                $this->sessionSvc->setSession('nombreProducto', $producto->getDescripcion());
                $this->sessionSvc->setSession('idProducto', $producto->getId());
                $this->sessionSvc->setSession('codigoProducto', $producto->getCodigo());
                return $this->redirect($this->generateUrl('precio_new'));
            }
        }

        if (isset($unidadActual) && $unidadActual != '') {
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findBy(array('unidadNegocio' => $unidadActual, 'estado' => 'A'));
        } else {
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findBy(array('estado' => 'A'));
        }
        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Precio:index.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
                    'listaPrecios' => $listaPrecios,
        ));
    }

    public function filtroAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        $producto = NULL;

        if ($request->get('codigo') == '') {
            $entities = $em->getRepository('JOYASJoyasBundle:Precio')->getAllByUnidad($request->get('unidadnegocio'));
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
            return $this->render('JOYASJoyasBundle:Precio:index.html.twig', array(
                        'entities' => $entities,
                        'unidades' => $unidades,
                        'listaPrecios' => $listaPrecios,
            ));
        }

        $this->sessionSvc->setSession('codigo', $request->get('codigo'));

        if ($request->get('codigo') != '000' and $this->sessionSvc->getSession('unidad') != '') {
            $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo'), 'unidadNegocio' => $this->sessionSvc->getSession('unidad')));
        } else {
            if ($this->sessionSvc->getSession('unidad') == '') {
                $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo'), 'unidadNegocio' => $request->get('unidadnegocio')));
            }
        }

        $this->sessionSvc->setSession('nombreProducto', $producto->getDescripcion());
        $this->sessionSvc->setSession('codigoProducto', $producto->getCodigo());

        if (!is_null($producto)) {
            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $entities = $em->getRepository('JOYASJoyasBundle:Precio')->findByProducto($producto->getId(), $this->sessionSvc->getSession('unidad'));
                $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas($this->sessionSvc->getSession('unidad'));
            } else {
                $entities = $em->getRepository('JOYASJoyasBundle:Precio')->findByProducto($producto->getId());
                $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
            }

            if (count($entities) > 1) {
                return $this->render('JOYASJoyasBundle:Precio:index.html.twig', array(
                            'entities' => $entities,
                            'unidades' => $unidades,
                            'listaPrecios' => $listaPrecios,
                ));
            }
            if (count($entities) == 1) {
                foreach ($entities as $entidad) {
                    $idPrecio = $entidad->getId();
                }
                return $this->redirect($this->generateUrl('precio_edit', array('id' => $idPrecio)));
            }
            if (count($entities) < 1) {
                $this->sessionSvc->setSession('nombreProducto', $producto->getDescripcion());
                $this->sessionSvc->setSession('idProducto', $producto->getId());
                $this->sessionSvc->setSession('codigoProducto', $producto->getCodigo());
                return $this->redirect($this->generateUrl('precio_new'));
            }
        } else {
            $this->sessionSvc->addFlash('msgWarn', 'No se han encontrado resultados.');
            return $this->redirect($this->generateUrl('precio'));
        }
    }

    /**
     *
     */
    public function filtroLAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $entities = $em->getRepository('JOYASJoyasBundle:Precio')->findByLista($request->get('listado'));
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:Precio')->findByLista($request->get('listado'));
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        return $this->render('JOYASJoyasBundle:Precio:index.html.twig', array(
                    'entities' => $entities,
                    'unidades' => $unidades,
                    'listaPrecios' => $listaPrecios,
        ));
    }

    /**
     * Creates a new Precio entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $idUnidadNegocio = $request->get('unidadnegocio');
        $entity = new Precio();
        $form = $this->createCreateForm($entity, $idUnidadNegocio);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $this->sessionSvc->addFlash('msgOk', 'Precio dado de alta. Cod: ' . $entity->getProducto()->getCodigo());
            return $this->redirect($this->generateUrl('precio'));
        }

        return $this->render('JOYASJoyasBundle:Precio:new.html.twig', array(
                    'entity' => $entity,
                    'unidad' => $request->get('unidadnegocio'),
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Precio entity.
     *
     * @param Precio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Precio $entity, $unidad = '') {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidad = $this->sessionSvc->getSession('unidad');
        }

        $form = $this->createForm(new PrecioType($unidad), $entity, array(
            'action' => $this->generateUrl('precio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first crear')));

        return $form;
    }

    /**
     * Displays a form to create a new Precio entity.
     *
     */
    public function newAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $entity = new Precio();
        $form = $this->createCreateForm($entity, $request->get('unidadnegocio'));
        return $this->render('JOYASJoyasBundle:Precio:new.html.twig', array(
                    'entity' => $entity,
                    'unidad' => $request->get('unidadnegocio'),
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Precio entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Precio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Precio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Precio:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Precio entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Precio')->find($id);

        $editForm = $this->createEditForm($entity, $entity->getListaPrecio()->getUnidadNegocio()->getId());

        return $this->render('JOYASJoyasBundle:Precio:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Precio entity.
     *
     * @param Precio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Precio $entity, $unidad = '') {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new PrecioType($unidad), $entity, array(
            'action' => $this->generateUrl('precio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Precio entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Precio')->find($id);

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity, $request->get('unidadnegocio'));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->sessionSvc->addFlash('msgOk', 'Se ha modificado el precio. Cod: ' . $entity->getProducto()->getCodigo());
            return $this->redirect($this->generateUrl('precio'));
        }

        return $this->render('JOYASJoyasBundle:Precio:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Precio entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Precio')->find($id);

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('precio'));
    }

    /**
     * Creates a form to delete a Precio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('precio_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Borrar', 'attr' => array('class' => 'btn')))
                        ->getForm()
        ;
    }

    public function predictivaAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $search = $request->get('data');
        $idlista = $request->get('data3');
        $string = '<ul>';

        if (strlen($search) >= 3) {
            $precios = $em->getRepository('JOYASJoyasBundle:Precio')->predictiva($search, $idlista);
            foreach ($precios as $precio) {
                $string = $string . '<li class="suggest-element pointer " data-codigo="' . $precio->getProducto()->getCodigo() . '" data-stock="' . $precio->getProducto()->getStock() . '" data-descripcion="' . $precio->getProducto()->getDescripcion() . '" data-precio="' . $precio->getValor() . '" data-id="' . $precio->getProducto()->getId() . '"><a>' . $precio->getProducto()->getDescripcion() . ' ( ' . $precio->getProducto()->getCodigo() . ' ) </a></li>';
            }
        }

        $response = $string;
        return new Response($response);
    }

    public function productoAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $codigo = $request->get('data');
        $idlista = $request->get('data3');
        $resultado = array();

        $precio = $em->getRepository('JOYASJoyasBundle:Precio')->producto($codigo, $idlista);
        if (count($precio)==1){
            $resultado['id']= $precio[0]->getProducto()->getId();
            $resultado['codigo']= $precio[0]->getProducto()->getCodigo();
            $resultado['descripcion']= $precio[0]->getProducto()->getDescripcion();
            $resultado['stock']= $precio[0]->getProducto()->getStock();
            $resultado['precio']= $precio[0]->getValor();
        }
        
        return new Response(json_encode($resultado));
    }

}
