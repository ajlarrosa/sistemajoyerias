<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use JOYAS\JoyasBundle\Entity\ListaPrecio;
use JOYAS\JoyasBundle\Entity\Precio;
use JOYAS\JoyasBundle\Entity\Producto;

class DefaultController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    public function indexAction() {
        $this->sessionSvc->startSession();
        return $this->render('JOYASJoyasBundle:Default:index.html.twig');
    }

    public function generarAction() {
        $hoy = new \DateTime('NOW');

        $pdfGenerator = $this->get('siphoc.pdf.generator');
        $pdfGenerator->setName('imprimirPrueba' . $hoy->format('Y-m-d H:i:s') . '.pdf');
        return $pdfGenerator->downloadFromView('JOYASJoyasBundle:Default:imprimir.html.twig');
    }

    public function inicioAction() {

        if ($this->sessionSvc->getSession('login') != 'false') {
            $this->sessionSvc->setSession('modulo', '');
            return $this->render('JOYASJoyasBundle:Default:inicio.html.twig');
        }

        $this->sessionSvc->startSession();
        return $this->render('JOYASJoyasBundle:Default:index.html.twig');
    }

    public function loginAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();

        $login = $request->get('usuario');
        $pass = $request->get('contrasena');

        if ($this->sessionSvc->login($login, $pass)) {
            return $this->redirect($this->generateUrl('joyas_joyas_inicio'));
        }
        return $this->render('JOYASJoyasBundle:Default:index.html.twig');
    }

    public function cerrarAction() {
        $this->sessionSvc->closeSession();
        return $this->render('JOYASJoyasBundle:Default:index.html.twig');
    }

    public function menuAction($tipoMenu) {

        $this->sessionSvc->setSession('modulo', $tipoMenu);

        if ($tipoMenu == 'clienteproveedorM') {
            return $this->redirect($this->generateUrl('clienteproveedor'));
        } else {
            if ($tipoMenu == 'consignacionesM') {
                return $this->redirect($this->generateUrl('consignacion'));
            } else {
                return $this->render('JOYASJoyasBundle:Default:inicio.html.twig');
            }
        }
    }

    public function restClaveAction(Request $request) {
        return $this->render('JOYASJoyasBundle:Default:olvideClave.html.twig');
    }

    public function olvideClaveAction(Request $request) {
        $username = $request->get('usuario');
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('JOYASJoyasBundle:Usuario')->findOneBy(array('login' => $username));
        if (!is_null($usuario)) {
            $mail = $usuario->getMail();
            if ($mail != '' and isset($mail)) {
                //crear mail y mandar
                $message = \Swift_Message::newInstance()
                        ->setSubject('Recuperacion de clave')
                        ->setFrom($this->container->getParameter('envio_mails'))
                        ->setTo($usuario->getMail())
                        ->setBody(
                        $this->renderView('JOYASJoyasBundle:Default:email.txt.twig', array('login' => $usuario->getLogin(), 'pass' => $usuario->getClave()))
                );
                $this->get('mailer')->send($message);

                //mensaje ok
                $this->sessionSvc->addFlash('msgOk', 'Su contraseña ha sido enviada a su dirección de correo electrónico.');
            } else {
                $this->sessionSvc->addFlash('msgOk', 'No pudimos reestablecer su clave, ya que no tiene mail.');
            }
        } else {
            $this->sessionSvc->addFlash('msgErr', 'Usuario inexistente.');
        }
        return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }

    public function cargaMasivaAction() {
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        return $this->render('JOYASJoyasBundle:Default:cargamasiva.html.twig', array(
                    'unidades' => $unidades,
        ));
    }

    Private function getLetra($columna) {
        switch ($columna) {
            case 1:
                return 'A';
            case 2:
                return 'B';
            case 3:
                return 'C';
            case 4:
                return 'D';
            case 5:
                return 'E';
            case 6:
                return 'F';
            case 7:
                return 'G';
            case 8:
                return 'H';
            case 9:
                return 'I';
            case 10:
                return 'J';
            case 11:
                return 'K';
            case 12:
                return 'L';
            case 13:
                return 'M';
            case 14:
                return 'N';
        }
    }

    public function importarArchivosAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $uploaddir = $this->get('kernel')->getRootDir() . "/../web/uploads/documents/";
        // Cell caching ::
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
        $filename = '';
        $a = $request->get('desde');
        $b = $a + 100;

        if ($a == 1) {
            if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
            } else {
                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionSvc->getSession('unidad'));
            }

            $this->sessionSvc->setSession('unidad', $unidad->getId());

            $filename = trim($_FILES['archivo']['name']);
            $uploadfile = $uploaddir . $filename;
            if (!move_uploaded_file($_FILES['archivo']['tmp_name'], $uploadfile)) {                          
                $this->sessionSvc->addFlash('msgError', 'No se pudo cargar el archivo.');
            }
        } else {
            $this->sessionSvc->setSession('unidad', $request->get('unidad'));
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidad'));
            $this->sessionSvc->setSession('unidadimp', $request->get('unidadimp'));
            $uploadfile = $this->sessionSvc->getSession('uploadfile');
        }

        if (!file_exists($uploadfile)) {
            $this->sessionSvc->addFlash('msgError', 'No se pudo migrar el archivo.');
            return $this->redirect($this->generateUrl('joyas_joyas_cargamasiva'));
        }

        ini_set('memory_limit', '-1');
        $inputFileType = 'Excel2007';
        $sheetname = 'Hoja1';
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setLoadSheetsOnly($sheetname);
        $objPHPExcel = $objReader->load($uploadfile);
        $vacio = false;

        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            for ($i = $a; $i <= $b; $i++) {
                if (!$vacio && $i > 1) {
                    for ($columnas = 1; $columnas < 16; $columnas++) {
                        $letracont = $this->getLetra($columnas);
                        if ($columnas == 1) {
                            $categoriasubcategoria = null;
                            $id = $worksheet->getCell($letracont . $i)->getCalculatedValue();
                            $categoriasubcategoria = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->findOneBy(array('id' => $id));
                            if (!isset($categoriasubcategoria)) {
                                $this->sessionSvc->addFlash('msgError', 'La línea ' . $i . ' no tiene id de categoria-subcategoria');
                                break;
                            }
                        }
                        if ($columnas == 2) {
                            $codigo = $worksheet->getCell($letracont . $i)->getCalculatedValue();
                            if ($codigo == '' || !isset($codigo)) {
                                $this->sessionSvc->addFlash('msgError', 'La línea ' . $i . ' no tiene código');
                                break;
                            }
                            $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio' => $unidad->getId()));
                            if (is_null($producto)) {
                                $producto = new Producto();
                                $producto->setUnidadNegocio($unidad);
                                $producto->setCodigo($codigo);
                                if (!is_null($categoriasubcategoria)) {
                                    $producto->setCategoriasubcategoria($categoriasubcategoria);
                                }
                            } else {
                                if (!is_null($categoriasubcategoria)) {
                                    $producto->setCategoriasubcategoria($categoriasubcategoria);
                                }
                            }
                        }
                        if ($columnas == 3) {
                            $stock = str_replace(',', '.', $worksheet->getCell($letracont . $i)->getCalculatedValue());
                            $producto->setStock($stock);
                        }
                        if ($columnas == 4) {
                            $producto->setCosto(str_replace(',', '.', $worksheet->getCell($letracont . $i)->getCalculatedValue()));
                        }
                        if ($columnas == 5) {
                            if (strtoupper($worksheet->getCell($letracont . $i)->getCalculatedValue()) == 'USD') {
                                $producto->setMoneda('2');
                            } else {
                                $producto->setMoneda('1');
                            }
                        }
                        if ($columnas == 6) {
                            $producto->setPlata(str_replace(',', '.', $worksheet->getCell($letracont . $i)->getCalculatedValue()));
                        }
                        if ($columnas == 7) {
                            $producto->setOro(str_replace(',', '.', $worksheet->getCell($letracont . $i)->getCalculatedValue()));
                        }
                        if ($columnas == 8) {
                            $producto->setQuilate(str_replace(',', '.', $worksheet->getCell($letracont . $i)->getCalculatedValue()));
                        }
                        
                        if ($columnas == 9) {
                            $producto->setDescripcion($worksheet->getCell($letracont . $i)->getCalculatedValue());
                            $em->persist($producto);
                            $em->flush();
                        }
                        if ($columnas == 10) {
                            
                        }

                        if ($columnas > 10) {
                            $descLista = $worksheet->getCell($letracont . '1')->getCalculatedValue();

                            if (!isset($descLista)) {
                                break;
                            }

                            $listaPrecio = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findOneBy(array('descripcion' => $descLista, 'unidadNegocio' => $unidad->getId()));
                           
                            if (!isset($listaPrecio)) {
                                $listaPrecio = new ListaPrecio();
                                $listaPrecio->setDescripcion($descLista);
                                $listaPrecio->setUnidadNegocio($unidad);

                                $cellMoneda = $worksheet->getCell('E2');
                                if ($cellMoneda->getCalculatedValue() == 'US') {
                                    $listaPrecio->setMoneda('2');
                                } else {
                                    $listaPrecio->setMoneda('1');
                                }
                                $em->persist($listaPrecio);
                                $em->flush();
                            }

                            if (trim($worksheet->getCell($letracont . $i)->getCalculatedValue()) != '') {
                                $precio = $em->getRepository('JOYASJoyasBundle:Precio')->findOneBy(array('producto' => $producto, 'listaPrecio' => $listaPrecio));
                                if (!isset($precio)) {
                                    $precio = new Precio();
                                    $precio->setListaPrecio($listaPrecio);
                                    $precio->setProducto($producto);
                                }                                 
                                $precio->setValor(str_replace(',', '.', $worksheet->getCell($letracont . $i)->getCalculatedValue()));                                
                                $em->persist($precio);
                                $em->flush();
                            }
                        }
                    }
                }
                $j=$i+1;
                $control = $worksheet->getCell('B' . $j)->getCalculatedValue();
                if (!isset($control)) {
                    $vacio = true;
                }

                $cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
                $deleted = $cacheDriver->deleteAll();
            }
            // ENVIAR DATOS
            if (!$vacio) {
                $this->sessionSvc->setSession('desde', $i);
                $this->sessionSvc->setSession('fin', 'no');
                $this->sessionSvc->setSession('uploadfile', $uploadfile);
                $this->sessionSvc->setSession('unidad', $unidad->getId());
                $this->sessionSvc->addFlash('msgWarn', 'Se ha actualizado desde la línea ' . $a . ' hasta la línea ' . $i . '. Y continuando...');
                return $this->redirect($this->generateUrl('joyas_joyas_inicio'));
            } else {
                $this->sessionSvc->addFlash('msgOk', 'Carga satisfactoria. Última línea: ' . $i);
                $this->sessionSvc->setSession('desde', '');
                $this->sessionSvc->setSession('fin', '');
                $this->sessionSvc->setSession('uploadfile', '');
            }
        }
        return $this->redirect($this->generateUrl('joyas_joyas_cargamasiva'));
    }

}

function decryptIt($q) {
    $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
    $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
    return( $qDecoded );
}

?>
