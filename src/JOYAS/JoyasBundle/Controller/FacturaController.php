<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\Factura;
use JOYAS\JoyasBundle\Entity\ProductoFactura;
use JOYAS\JoyasBundle\Entity\MovimientoCC;
use JOYAS\JoyasBundle\Form\FacturaType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Factura controller.
 *
 */
class FacturaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Factura entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $unidadActual = $this->sessionSvc->getSession('unidad');
        $unidad = $request->get('unidadNegocio');
        $cliente = $request->get('cliente');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bCliente', $cliente);
        $this->sessionSvc->setSession('bFechaDesde', $fechaDesde);
        $this->sessionSvc->setSession('bFechaHasta', $fechaHasta);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Factura')->filtroBusqueda($unidad, $fechaDesde, $fechaHasta, $cliente, $unidadActual);

        if (isset($unidadActual) && $unidadActual != '') {
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findBy(array('unidadNegocio' => $unidadActual, 'estado' => 'A'));
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($unidadActual);
        } else {
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findBy(array('estado' => 'A'));
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
        }

        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Factura:index.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
                    'clientesProveedores' => $clientesProveedores,
                    'listaPrecios' => $listaPrecios
        ));
    }

    public function filtroAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $facturas = $em->getRepository('JOYASJoyasBundle:Factura')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            if ($request->get('unidadnegocio') != '0') {
                $facturas = $em->getRepository('JOYASJoyasBundle:Factura')->getAllActivas($request->get('unidadnegocio'));
            } else {
                $facturas = $em->getRepository('JOYASJoyasBundle:Factura')->getAllActivas();
            }
        }

        $entities = new ArrayCollection();

        $desde = new \DateTime($request->get('fechaDesde'));
        $hasta = new \DateTime($request->get('fechaHasta'));
        $listado = $request->get('listado');

        foreach ($facturas as $fac) {
            $fecha = $fac->getFecha();

            if ($request->get('fechaDesde') != '' and $request->get('fechaHasta') != '') {
                if ($desde <= $fecha and $fac->getFecha() <= $hasta) {
                    if ($listado == '0') {
                        $entities->add($fac);
                    } else {
                        if ($place != 'gastos' and $fac->getMovimientoCC()->getTipoDocumento() != 'G'and $fac->getMovimientoCC()->getTipoDocumento() != 'A' and $fac->getMovimientoCC()->getTipoDocumento() != 'FC') {
                            if ($listado == $fac->getMovimientoCC()->getClienteProveedor()->getId()) {
                                $entities->add($fac);
                            }
                        }
                    }
                }
            } else {
                if ($listado != '0') {
                    if ($fac->getMovimientoCC()->getTipoDocumento() == 'FC') {
                        if ($listado == $fac->getMovimientoCC()->getClienteProveedor()->getId()) {
                            $entities->add($fac);
                        }
                    }
                }
            }
        }

        if ($request->get('unidadnegocio') != '0' and $listado == '0' and $request->get('fechaDesde') == '' and $request->get('fechaHasta') == '' and count($entities) < 1 and count($facturas) > 0) {
            $entities = $facturas;
        }

        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            if ($request->get('unidadnegocio') == '0' and $listado == '0' and $request->get('fechaDesde') == '' and $request->get('fechaHasta') == '' and count($entities) < 1 and count($facturas) > 0) {
                $entities = $em->getRepository('JOYASJoyasBundle:Factura')->getAllActivas();
            }
        }

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($this->sessionSvc->getSession('unidad'));
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
        }

        return $this->render('JOYASJoyasBundle:Factura:index.html.twig', array(
                    'entities' => $entities,
                    'clientesProveedores' => $clientesProveedores,
                    'unidades' => $unidades,
                    'listaPrecios' => $listaPrecios,
        ));
    }

    /**
     * Creates a new Factura entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        $movimientocc = new MovimientoCC();

        $consignacion = $request->get('consignacion');

        $cliPro = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($request->get('cliente'));
        $listaPrecio = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($request->get('listaPrecio'));
        $entity->setDescuento($request->get('descuento'));
        $entity->setFecha(new \DateTime());
        $entity->setListaPrecio($listaPrecio);
        $entity->setBonificacion($request->get('bonificacion'));
        $entity->setImporte($request->get('resultadoFinal'));
        $entity->setOro($request->get('oro'));
        $entity->setPlata($request->get('plata'));
        $em->persist($entity);
        $em->flush();

        $movimientocc->setClienteProveedor($cliPro);
        $movimientocc->setFactura($entity);
        $movimientocc->settarjeta($request->get('tarjeta'));
        $movimientocc->setTipoDocumento('FC');
        $movimientocc->setMoneda($listaPrecio->getMoneda());

        $movimientocc->setUnidadNegocio($listaPrecio->getUnidadNegocio());

        $em->persist($movimientocc);
        $em->flush();

        $contador = $request->get('contador');

        for ($x = 0; $x <= $contador; $x++) {
            $codigo = $request->get('codigo' . $x);
            $desc = $request->get('descripcion' . $x);

            if (!is_null($codigo) and $codigo != '' and $desc != '' and ! is_null($desc)) {
                $productoFactura = new ProductoFactura();

                if ($codigo != '000') {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio' => $entity->getListaPrecio()->getUnidadNegocio()));
                } else {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo));
                }

                if (!is_null($producto) && $codigo != '000' && $consignacion != 'si') {
                    $stock = $producto->getStock() - $request->get('cantidad' . $x);
                    if ($stock < 0) {
                        $stock = 0;
                    }
                    $producto->setStock($stock);
                }

                if (!is_null($producto)) {
                    $productoFactura->setProducto($producto);
                    $productoFactura->setFactura($entity);
                    $productoFactura->setPrecio($request->get('precio' . $x));
                    $productoFactura->setCantidad($request->get('cantidad' . $x));
                    $entity->addProductosFactura($productoFactura);
                }
            }
        }

        if ($consignacion == 'si' and $request->get('tipo') == 'total') {
            $cons = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($request->get('idCons'));
            foreach ($cons->getProductosConsignacion() as $prodCon) {
                $em->remove($prodCon);
                $em->flush();
            }
            $em->remove($cons);
            $em->flush();
        }

        if ($consignacion == 'si' and $request->get('tipo') == 'parcial') {
            $cons = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($request->get('idCons'));
            $totales = 'si';
            $tot = 0;
            for ($x = 0; $x <= $contador; $x++) {
                $codigo = $request->get('codigo' . $x);
                $cantidad = $request->get('cantidad' . $x);
                foreach ($cons->getProductosConsignacion() as $prodCon) {
                    if ($prodCon->getProducto()->getCodigo() == $codigo) {
                        if ($cantidad == $prodCon->getCantidad()) {
                            $em->remove($prodCon);
                            $em->flush();
                            $tott = $cons->getImporte() - ($cantidad * $prodCon->getPrecio());
                            $cons->setImporte($tott);
                            break;
                        } else {
                            if ($cantidad < $prodCon->getCantidad()) {
                                $resta = $prodCon->getCantidad() - $cantidad;
                                $prodCon->setCantidad($resta);
                                $totales = 'no';
                                $tott = $cons->getImporte() - ($cantidad * $prodCon->getPrecio());
                                $cons->setImporte($tott);
                                break;
                            }
                        }
                    }
                }
            }
            $em->flush();

            if ($totales == 'si' and count($cons->getProductosConsignacion()) < 1) {
                $em->remove($cons);
                $em->flush();
            }
        }

        if (count($entity->getProductosFactura()) < 1) {
            $em->remove($entity);
            $em->remove($movimientocc);
            $em->flush();

            $this->sessionSvc->addFlash('msgError', 'La factura debe tener al menos un producto.');

            $this->sessionSvc->setSession('listaprecio', $listaPrecio->getId());

            return $this->redirect($this->generateUrl('factura_new'));
        }

        $entity->setMovimientocc($movimientocc);
        $em->flush();

        $this->sessionSvc->setSession('listaprecio', '');
        $this->sessionSvc->addFlash('msgOk', 'Venta registrada.');

        return $this->redirect($this->generateUrl('factura_show', array('id' => $entity->getId())));
    }

    /**
     * Creates a form to create a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Factura $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new FacturaType(), $entity, array(
            'action' => $this->generateUrl('factura_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first crear')));

        return $form;
    }

    /**
     * Displays a form to create a new Factura entity.
     *
     */
    public function newAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        if ($this->sessionSvc->getSession('listaprecio') == '') {
            $this->sessionSvc->setSession('listaprecio', $request->get('listaprecio'));
        } else {
            $set = $request->get('listaprecio');
            if ($set != '' and isset($set) and $this->sessionSvc->getSession('listaprecio') != $request->get('listaprecio')) {
                $this->sessionSvc->setSession('listaprecio', $request->get('listaprecio'));
            }
        }
        
        $em = $this->getDoctrine()->getManager();

        $listaPrecio = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($this->sessionSvc->getSession('listaprecio'));
        $clientes = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('unidadNegocio'=>$listaPrecio->getUnidadNegocio(),'estado'=>'A','clienteProveedor'=>1,),array('razonSocial'=>'ASC'));        

        return $this->render('JOYASJoyasBundle:Factura:new.html.twig', array(                   
                    'clientes' => $clientes,
                    'listaPrecio' => $listaPrecio,
        ));
    }

    public function parcialAction(Request $request) {

        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $consignacion = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($request->get('idCons'));

        $contador = $request->get('contador');
        $productosConsignacion = new ArrayCollection();

        for ($x = 0; $x <= $contador; $x++) {
            $check = $request->get('checkbox' . $x);
            $this->sessionSvc->setSession('check' . $x, $check);
            if ($check == 'on') {
                $codigo = $request->get('codigo' . $x);
                foreach ($consignacion->getProductosConsignacion() as $pCv) {
                    if ($pCv->getProducto()->getCodigo() == $codigo) {
                        $productosConsignacion->add($pCv);
                        break;
                    }
                }
            }
        }

        $entity = new Factura();

        return $this->render('JOYASJoyasBundle:Factura:newParcial.html.twig', array(
                    'entity' => $entity,
                    'consignacion' => $consignacion,
                    'productosConsignacion' => $productosConsignacion,
        ));
    }

    /**
     * Finds and displays a Factura entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }
        return $this->render('JOYASJoyasBundle:Factura:show.html.twig', array(
                    'entity' => $entity,));
    }

    public function imprimirAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }


        return $this->render('JOYASJoyasBundle:Factura:imprimir.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Factura:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Factura $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new FacturaType(), $entity, array(
            'action' => $this->generateUrl('factura_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Factura entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('factura_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Factura:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Factura entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Factura entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('factura'));
    }

    /**
     * Creates a form to delete a Factura entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('factura_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
