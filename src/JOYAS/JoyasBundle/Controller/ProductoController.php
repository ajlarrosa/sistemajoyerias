<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\Precio;
use JOYAS\JoyasBundle\Entity\ImagenProducto;
use JOYAS\JoyasBundle\Entity\Producto;
use JOYAS\JoyasBundle\Entity\ValorTipoCosto;
use JOYAS\JoyasBundle\Form\ProductoType;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\Resize;
use JOYAS\JoyasBundle\Entity\Cotizacion;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Producto controller.
 *
 */
class ProductoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Producto entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $unidad = $request->get('unidadNegocio');
        $busqueda = $request->get('busqueda');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bBusqueda', $busqueda);
        $em = $this->getDoctrine()->getManager();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidadNegocio = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionSvc->getSession('unidad'));

            if ($unidadNegocio->getUsaParametrica()) {
                $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->findBy(array('estado' => 'A', 'unidadNegocio' => $unidadNegocio), array('id' => 'DESC'), 1);
            } else {
                $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->findBy(array('estado' => 'A', 'unidadNegocio' => null), array('id' => 'DESC'), 1);
            }
        } else {
            $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->findBy(array('estado' => 'A'), array('id' => 'DESC'), 1);
        }
        //Para obtener la primera cotizacion 
        //que es la ultima ingresada
        if (!isset($cotizacion[0])) {
            //Si no tiene la unidad de Negocio asignada una cotizacion
            //Entonces simplemente toma la primera de todas
            $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->find(1);
        } else {
            $cotizacion = $cotizacion[0];
        }

        $entities = $em->getRepository('JOYASJoyasBundle:Producto')->filtroBusqueda($busqueda, $this->sessionSvc->getSession('unidad'), $unidad);

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findBy(array('estado' => 'A'), array('descripcion' => 'DESC'));

        return $this->render('JOYASJoyasBundle:Producto:index.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
                    'cotizacion' => $cotizacion
        ));
    }

    /**
     * Lists all Producto entities.
     *
     */
    public function ajusteAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $unidadActual = $this->sessionSvc->getSession('unidad');
        $unidad = $request->get('unidadNegocio');
        $busqueda = $request->get('busqueda');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bBusqueda', $busqueda);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Producto')->filtroBusqueda($busqueda, $unidadActual, $unidad);

        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Producto:ajuste.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades,
        ));
    }

    /**
     *
     */
    public function filtroAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $producto = NULL;

        if ($request->get('codigo') != '000' and $this->sessionSvc->getSession('unidad') != '') {
            $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo'), 'unidadNegocio' => $this->sessionSvc->getSession('unidad')));
        } else {
            if ($this->sessionSvc->getSession('unidad') == '') {
                $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo')));
            }
        }

        if (!is_null($producto)) {
            $entities = new ArrayCollection();
            $entities->add($producto);
            return $this->render('JOYASJoyasBundle:Producto:ajuste.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->sessionSvc->addFlash('msgWarn', 'No se han encontrado resultados.');
            return $this->redirect($this->generateUrl('producto_ajuste'));
        }
    }

    /**
     *
     */
    public function filtroPAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $producto = NULL;
        $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->getUltima();

        if ($request->get('codigo') == '' and $request->get('unidadnegocio') != '' and $this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $entities = $em->getRepository('JOYASJoyasBundle:Producto')->getAllActivas($request->get('unidadnegocio'));
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
            return $this->render('JOYASJoyasBundle:Producto:index.html.twig', array(
                        'entities' => $entities,
                        'cotizacion' => $cotizacion,
                        'unidades' => $unidades,
            ));
        }

        if ($request->get('codigo') != '000' and $this->sessionSvc->getSession('unidad') != '') {
            $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo'), 'unidadNegocio' => $this->sessionSvc->getSession('unidad')));
        } else {
            if ($this->sessionSvc->getSession('unidad') == '') {
                $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $request->get('codigo'), 'unidadNegocio' => $request->get('unidadnegocio')));
            }
        }

        $entities = new ArrayCollection();
        $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->getUltima();
        if (!is_null($producto)) {
            $entities->add($producto);

            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

            return $this->render('JOYASJoyasBundle:Producto:index.html.twig', array(
                        'entities' => $entities,
                        'unidades' => $unidades,
                        'cotizacion' => $cotizacion,
            ));
        } else {
            return $this->redirect($this->generateUrl('producto'));
        }
    }

    /**
     * Creates a new Producto entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $entity = new Producto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionSvc->getSession('unidad'));
        } else {
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
        }
        $error = 'no';
        $entity->setUnidadNegocio($unidad);
        $codigo = $entity->getCodigo();
        if (isset($codigo) and $codigo != '') {
            $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $entity->getCodigo(), 'unidadNegocio' => $unidad->getId()));
            if (!is_null($producto)) {
                $error = 'si';
            }
        }

        if ($error == 'no' and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $entity->setCodigo($codigo);
            $em->flush();

            $uploaddir = "uploads/documents/productos/" . $entity->getId();
            if (!file_exists($uploaddir)) {
                mkdir($uploaddir, 0777, true);
            }
            $archivo = '';
            for ($i = 1; $i < 6; $i++) {
                $archivo = 'archivo' . $i;
                $filename = trim($_FILES[$archivo]['name']);
                $filesize = $_FILES[$archivo]['size'];
                if ($filesize > 0) {
                    $uploadfile = $uploaddir . $filename;

                    $tmp_name = $_FILES[$archivo]["tmp_name"];
                    $name = $_FILES[$archivo]["name"];

                    if (move_uploaded_file($tmp_name, "$uploaddir/$name")) {
                        $obj = new Resize();
                        $obj->max_width(600);
                        $obj->max_height(600);
                        $obj->image_path($this->get('kernel')->getRootDir() . "/../web/$uploaddir/$name");
                        $obj->image_resize();

                        $imagenproducto = new ImagenProducto();
                        $imagenproducto->setPath($name);
                        $imagenproducto->setProducto($entity);
                        $em->persist($imagenproducto);
                        $em->flush();
                    }
                }
            }

            $this->sessionSvc->addFlash('msgOk', 'Alta satisfactoria, puede continuar.');

            $entity = new Producto();
            $form = $this->createCreateForm($entity);
            return $this->render('JOYASJoyasBundle:Producto:new.html.twig', array(
                        'entity' => $entity,
                        'unidades' => $unidades,
                        'form' => $form->createView(),
            ));
        }

        if ($form->isValid()) {
            $this->sessionSvc->addFlash('msgError', 'El código debe ser único.');
        }
        return $this->render('JOYASJoyasBundle:Producto:new.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Producto entity.
     *
     * @param Producto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Producto $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $form = $this->createForm(new ProductoType(), $entity, array(
            'action' => $this->generateUrl('producto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Displays a form to create a new Producto entity.
     *
     */
    public function newAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = new Producto();
        $form = $this->createCreateForm($entity);
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        return $this->render('JOYASJoyasBundle:Producto:new.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Producto entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Producto:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function vistarapidaAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $codigo = $request->get('codigo');

        $entity = null;

        if (!empty($codigo)) {
            $entity = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array("codigo" => $codigo));
            if (is_null($entity)) {
                $this->sessionSvc->addFlash("msgError", "No se encontraron resultados");
            }
        }

        return $this->render('JOYASJoyasBundle:Producto:vistarapida.html.twig', array(
                    'entity' => $entity,
        ));
    }

    public function webAction($id) {
       /*if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }*/

        $em = $this->getDoctrine()->getManager();
        $lista = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findOneBy(array('descripcion' => 'Lista Web'));
        $entity = $em->getRepository('JOYASJoyasBundle:Precio')->findOneBy(array('producto' => $id, 'listaPrecio' => $lista->getId()));

        if (!$entity->getProducto()->getVisible()) {
            $this->sessionSvc->addFlash('msgWarn', 'No puede visualizarse el producto');
            return $this->redirect($this->generateUrl('joyas_joyas_web'));
        }
        if (!is_null($entity->getProducto()->getCategoriasubcategoria())) {
            $relacionados = $em->getRepository('JOYASJoyasBundle:Producto')->relacionados($entity->getProducto()->getCategoriasubcategoria()->getId());
        } else {
            $relacionados = new ArrayCollection();
        }
        return $this->render('JOYASJoyasBundle:Web:single.html.twig', array(
                    'entity' => $entity,
                    'relacionados' => $relacionados
        ));
    }

    public function categoriasAction($id) {
        /*if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }*/

        $em = $this->getDoctrine()->getManager();
        $categoria = $em->getRepository('JOYASJoyasBundle:Categoria')->find($id);
        $subcategorias = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->porcategoria($id);
        $precios = $em->getRepository('JOYASJoyasBundle:Producto')->porcategoria($id);

        return $this->render('JOYASJoyasBundle:Web:categorias.html.twig', array(
                    'categoria' => $categoria,
                    'subcategorias' => $subcategorias,
                    'precios' => $precios,
        ));
    }

    public function filtropreciosAction(Request $request) {
        /*if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }*/

        $em = $this->getDoctrine()->getManager();
        $id = $request->get('categoria');

        $rangos = explode('-', $request->get('filtro'));
        if ($request->get('pantalla') == 'categoria') {
            $categoria = $em->getRepository('JOYASJoyasBundle:Categoria')->find($id);
            $subcategorias = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->porcategoria($id);
            $precios = $em->getRepository('JOYASJoyasBundle:Producto')->porcategoriayprecio($id, $rangos[0], $rangos[1]);
            return $this->render('JOYASJoyasBundle:Web:categorias.html.twig', array(
                        'categoria' => $categoria,
                        'subcategorias' => $subcategorias,
                        'precios' => $precios,
            ));
        }
        if ($request->get('pantalla') == 'novedades') {
            $precios = $em->getRepository('JOYASJoyasBundle:Producto')->novedadesxprecio($rangos[0], $rangos[1]);
            return $this->render('JOYASJoyasBundle:Web:novedades.html.twig', array(
                        'novedades' => $precios,
            ));
        }
        if ($request->get('pantalla') == 'ofertas') {
            $precios = $em->getRepository('JOYASJoyasBundle:Producto')->ofertasxprecio($rangos[0], $rangos[1]);
            return $this->render('JOYASJoyasBundle:Web:ofertas.html.twig', array(
                        'ofertas' => $precios,
            ));
        }

        return $this->redirect($this->generateUrl('joyas_joyas_web'));
    }

    public function buscarwebAction(Request $request) {
        /*if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }*/

        $desc = $request->get('buscar');

        $em = $this->getDoctrine()->getManager();

        $precios = $em->getRepository('JOYASJoyasBundle:Producto')->porbusqueda(trim($desc));

        return $this->render('JOYASJoyasBundle:Web:resultado.html.twig', array(
                    'precios' => $precios,
                    'busc' => $desc,
        ));
    }

    /**
     * Displays a form to edit an existing Producto entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Producto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Producto entity.
     *
     * @param Producto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Producto $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $form = $this->createForm(new ProductoType(), $entity, array(
            'action' => $this->generateUrl('producto_update', array('id' => $entity->getId())),
            'method' => 'post',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Producto entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);
        $conAnt = $entity->getCodigo();

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $error = 'no';

        if ($editForm->isValid()) {
            $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $entity->getCodigo(), 'unidadNegocio' => $entity->getUnidadNegocio()->getId()));

            if ($error == 'no') {
                $misDato = $request->get('checks');
                if ($misDato != '') {
                    foreach ($misDato as $mi_dato) {
                        $imagen = $em->getRepository('JOYASJoyasBundle:ImagenProducto')->find($mi_dato);
                        if (!is_null($imagen)) {
                            unlink("uploads/documents/productos/" . $id . "/" . $imagen->getPath());
                            $em->remove($imagen);
                            $em->flush();
                        }
                    }
                }
                $uploaddir = "uploads/documents/productos/" . $entity->getId();
                if (!file_exists($uploaddir)) {
                    mkdir($uploaddir, 0777, true);
                }
                $archivo = '';
                for ($i = 1; $i < 6; $i++) {
                    $archivo = 'archivo' . $i;
                    $filename = trim($_FILES[$archivo]['name']);
                    $filesize = $_FILES[$archivo]['size'];
                    if ($filesize > 0) {
                        $uploadfile = $uploaddir . $filename;

                        $tmp_name = $_FILES[$archivo]["tmp_name"];
                        $name = $_FILES[$archivo]["name"];

                        if (move_uploaded_file($tmp_name, "$uploaddir/$name")) {
                            $obj = new Resize();
                            $obj->max_width(600);
                            $obj->max_height(600);
                            $obj->image_path($this->get('kernel')->getRootDir() . "/../web/$uploaddir/$name");
                            $obj->image_resize();
                            $imagenproducto = new ImagenProducto();
                            $imagenproducto->setPath($name);
                            $imagenproducto->setProducto($entity);
                            $em->persist($imagenproducto);
                            $em->flush();
                        }
                    }
                }

                $em->flush();

                $this->sessionSvc->addFlash('msgOk', 'Se ha modificado el producto con codigo: ' . $entity->getCodigo());
                return $this->redirect($this->generateUrl('producto'));
            }
        }

        if ($editForm->isValid()) {
            $this->sessionSvc->addFlash('msgError', 'El codigo debe ser unico.');
        }

        return $this->render('JOYASJoyasBundle:Producto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Producto entity.
     *
     */
    public function stockAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }


        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);
        $stock = 'stock' . $id;
        $entity->setStock($request->get($stock));

        $em->flush();

        $this->sessionSvc->addFlash('msgOk', 'Se ha modificado el stock.');
        return $this->redirect($this->generateUrl('producto_ajuste'));
    }

    /**
     * Deletes a Producto entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }
        $entity->setEstado('E');
        $em->persist($entity);
        $em->flush();
        $this->sessionSvc->addFlash('msgOk', 'Se ha eliminado el producto con el código '.$entity->getCodigo());
        return $this->redirect($this->generateUrl('producto'));
    }

    /**
     * Creates a form to delete a Producto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('producto_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Borrar', 'attr' => array('class' => 'btn')))
                        ->getForm()
        ;
    }

    /**
     * Genera la pantalla para definir el costo apropiado al producto.
     *
     */
    public function costoAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);
        $categorias = $em->getRepository('JOYASJoyasBundle:Categoria')->findAll();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        return $this->render('JOYASJoyasBundle:Producto:costo.html.twig', array(
                    'entity' => $entity,
                    'categorias' => $categorias,
        ));
    }

    /**
     * Guarda el costo del producto.
     *
     */
    public function altaCostoAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

        $cantCostos = $request->request->get('cantCostos');

        if ($cantCostos > 0) {
            for ($i = 0; $i < $cantCostos; $i++) {

                $idCosto = 'idCosto' . $i;
                $costoValor = 'costoValor' . $i;
                $costoCantidad = 'costoCantidad' . $i;
                $resultado = 'resultado' . $i;

                $idTipoCosto = $request->request->get($idCosto);

                $tipoCosto = $em->getRepository('JOYASJoyasBundle:TipoCosto')->find($idTipoCosto);

                $valorTipoCosto = new ValorTipoCosto();
                $precio = new Precio();
                $hoy = new \DateTime();

                $precio->setValor($request->request->get('precio'));
                $precio->setFecha($hoy);
                $precio->setProducto($entity);

                $valorTipoCosto->setProducto($entity);
                $valorTipoCosto->setTipoCosto($tipoCosto);
                $valorTipoCosto->setCantidad($request->request->get($costoCantidad));
                $valorTipoCosto->setValor($request->request->get($costoValor));

                $em->persist($precio);
                $em->persist($valorTipoCosto);
                $em->flush();
            }

            $this->sessionSvc->addFlash('msgOk', 'Alta de Costo satisfactoria.');

            return $this->redirect($this->generateUrl('producto'));
        } else {

            $this->sessionSvc->addFlash('msgWarn', 'No existen tipos de costo para dar el alta.');
            $categorias = $em->getRepository('JOYASJoyasBundle:Categoria')->findAll();

            return $this->render('JOYASJoyasBundle:Producto:costo.html.twig', array(
                        'entity' => $entity,
            ));
        }
    }

    /**
     * Lists all Producto entities.
     *
     */
    private function getCostoValorizado($entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

//        $cotizacion = new Cotizacion();
        $em = $this->getDoctrine()->getManager();

        $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->getUltima();
        if ($entity->getMoneda() == 1) {
            switch ($entity->getTipoStock()) {
                case "U":
                    return $entity->getCosto() + ($entity->getOro() * $cotizacion->getOro()) + ($entity->getPlata() * $cotizacion->getPlata());
                case "P":
                    return round($entity->getCosto() + $cotizacion->getPlata());
                case "O":
                    return round($entity->getCosto() + $cotizacion->getOro());

                default:
                    return 0;
            }
        } else {
            switch ($entity->getTipoStock()) {
                case "U":
                    return $entity->getCosto() + ($entity->getOro() * $cotizacion->getOro() / $cotizacion->getDolar()) + ($entity->getPlata() * $cotizacion->getPlata() / $cotizacion->getDolar());
                case "P":
                    return round($entity->getCosto() + $entity->getPlata() * $cotizacion->getPlata() / $cotizacion->getDolar());
                case "O":
                    return round($entity->getCosto() + $entity->getOro() * $cotizacion->getOro() / $cotizacion->getDolar());

                default:
                    return 0;
            }
        }
    }

    public function informeProductoAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        return $this->render('JOYASJoyasBundle:Producto:informeProducto.html.twig', array(
                    'unidades' => $unidades
        ));
    }

    public function dashBoardProductoAction(Request $request, $unidad) {

        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $unidadN = $request->get('unidadN');
        if ($unidadN != '') {
            $unidad = $unidadN;
        }

        //Mostrar  DashBoard
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidad = $this->sessionSvc->getSession('unidad');
        }
        $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->getUltima();
        $totalCosto = $em->getRepository('JOYASJoyasBundle:Producto')->getTotalCosto($cotizacion, $unidad); //En pesos           
        $totalPlata = $em->getRepository('JOYASJoyasBundle:Producto')->getTotalPlata($unidad); //En gramos
        $totalOro = $em->getRepository('JOYASJoyasBundle:Producto')->getTotalOro($unidad); //En pesos  
        $totalUnidades = $em->getRepository('JOYASJoyasBundle:Producto')->getTotalUnidades($unidad); //En pesos             

        return $this->render('JOYASJoyasBundle:Producto:dashBoardProducto.html.twig', array(
                    'totalCosto' => $totalCosto,
                    'totalPlata' => $totalPlata,
                    'totalOro' => $totalOro,
                    'totalUnidades' => $totalUnidades,
                    'unidad' => $unidad
        ));
    }

    public function detalleUnidadesAction(Request $request, $unidad) {

        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        //$unidad = $request->get('unidad');
        //Mostrar  DashBoard
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidad = $this->sessionSvc->getSession('unidad');
        }
        $cantidadPorCategoria = $em->getRepository('JOYASJoyasBundle:Producto')->getCantidadPorCategoria($unidad); //En Unidades
        $cantidadPorCategoriaNull = $em->getRepository('JOYASJoyasBundle:Producto')->getCantidadPorCategoriaNull($unidad); //En Unidades        

        return $this->render('JOYASJoyasBundle:Producto:detalleUnidades.html.twig', array(
                    'cantidadPorCategoria' => $cantidadPorCategoria,
                    'cantidadPorCategoriaNull' => $cantidadPorCategoriaNull,
                    'unidad' => $unidad
        ));
    }

    public function detallePlataAction(Request $request, $unidad) {

        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        //Mostrar  DashBoard
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidad = $this->sessionSvc->getSession('unidad');
        }
        $cantidadPorCategoria = $em->getRepository('JOYASJoyasBundle:Producto')->getPlataPorCategoria($unidad); //En gramos            
        $cantidadPorCategoriaNull = $em->getRepository('JOYASJoyasBundle:Producto')->getPlataPorCategoriaNull($unidad); //En gramos        
        return $this->render('JOYASJoyasBundle:Producto:detallePlata.html.twig', array(
                    'cantidadPorCategoria' => $cantidadPorCategoria,
                    'cantidadPorCategoriaNull' => $cantidadPorCategoriaNull,
                    'unidad' => $unidad
        ));
    }

    public function detalleOroAction(Request $request, $unidad) {

        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        //Mostrar  DashBoard
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidad = $this->sessionSvc->getSession('unidad');
        }
        $cantidadPorCategoria = $em->getRepository('JOYASJoyasBundle:Producto')->getOroPorCategoria($unidad); //En gramos            
        $cantidadPorCategoriaNull = $em->getRepository('JOYASJoyasBundle:Producto')->getOroPorCategoriaNull($unidad); //En gramos        
        return $this->render('JOYASJoyasBundle:Producto:detalleOro.html.twig', array(
                    'cantidadPorCategoria' => $cantidadPorCategoria,
                    'cantidadPorCategoriaNull' => $cantidadPorCategoriaNull,
                    'unidad' => $unidad
        ));
    }

    public function detalleCostoAction(Request $request, $unidad) {

        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        //Mostrar  DashBoard
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidad = $this->sessionSvc->getSession('unidad');
        }
        $cotizacion = $em->getRepository('JOYASJoyasBundle:Cotizacion')->getUltima();
        $cantidadPorCategoria = $em->getRepository('JOYASJoyasBundle:Producto')->getCostoPorCategoria($unidad, $cotizacion); //En Pesos
        $cantidadPorCategoriaNull = $em->getRepository('JOYASJoyasBundle:Producto')->getCostoPorCategoriaNull($unidad, $cotizacion); //En Pesos       

        return $this->render('JOYASJoyasBundle:Producto:detalleCosto.html.twig', array(
                    'cantidadPorCategoria' => $cantidadPorCategoria,
                    'cantidadPorCategoriaNull' => $cantidadPorCategoriaNull,
                    'unidad' => $unidad
        ));
    }

    public function imprimirCodigoAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $cantidad = $_GET['num'];
        $codigo = $_GET['cod'];
        if (!isset($cantidad) or $cantidad == 0) {
            $cantidad = 1;
        }

        return $this->render('JOYASJoyasBundle:Producto:imprimirCodigo.html.twig', array(
                    'cantidad' => $cantidad,
                    'codigo' => $codigo,
        ));
    }

    public function carritoajaxAction(Request $request) {
        $search = $request->get('data');
        $this->sessionSvc->agregaralcarrito($search);
        $response = 'Producto agregado al carrito.';

        return new Response(json_encode($response));
    }

    public function quitarajaxAction(Request $request) {
        $search = $request->get('data');
        $this->sessionSvc->quitardelcarrito($search);
        $response = 'Producto sacado del carrito.';

        return new Response(json_encode($response));
    }

    public function vaciarajaxAction() {
        $this->sessionSvc->vaciarcarrito();
        $response = 'Se ha vaciado el carrito.';

        return new Response(json_encode($response));
    }

}
