<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\MovimientoCC;
use JOYAS\JoyasBundle\Form\MovimientoCCType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * MovimientoCC controller.
 *
 */
class MovimientoCCController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all MovimientoCC entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->findAll();

        return $this->render('JOYASJoyasBundle:MovimientoCC:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     *
     */
    public function admcajaAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $entities = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getAllActivasST($this->sessionSvc->getSession('unidad'));            
            $cheques = $em->getRepository('JOYASJoyasBundle:MovimientoCheque')->findBy(array('unidadNegocio'=>$this->sessionSvc->getSession('unidad')));
            //$cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array('unidadNegocio'=>$this->sessionSvc->getSession('unidad')),array('fecha'=>'ASC'));
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getAllActivasST();
            //$cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array(),array('fecha'=>'ASC'));            
            $cheques = $em->getRepository('JOYASJoyasBundle:MovimientoCheque')->findAll();            
            //$cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->adminCheque();
        }
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        return $this->render('JOYASJoyasBundle:MovimientoCC:admcaja.html.twig', array(
                    'entities' => $entities,
                    'cheques' => $cheques,
                    'unidades' => $unidades,
        ));
    }

    /**
     *
     */
    public function filtroAction(Request $request) {
        if ($this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $this->sessionSvc->setSession('cajade', '');

        $em = $this->getDoctrine()->getManager();
        if ($request->get('unidadnegocio') != '0') {
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
            $this->sessionSvc->setSession('cajade', $unidad->getDescripcion());
            $entities = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getAllActivas($request->get('unidadnegocio'));
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getAllActivas();
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        return $this->render('JOYASJoyasBundle:MovimientoCC:admcaja.html.twig', array(
                    'entities' => $entities,
                    'unidades' => $unidades,
        ));
    }

    public function imprimirAction() {
        if ($this->sessionSvc->isLogged()) {

            $em = $this->getDoctrine()->getManager();
            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $entities = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getAllActivas($this->sessionSvc->getSession('unidad'));
            } else {
                $entities = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getAllActivas();
            }

            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            $phpExcelObject->getProperties()->setCreator($this->container->getParameter('nombre_cliente'))
                    ->setLastModifiedBy($this->container->getParameter('nombre_cliente'))
                    ->setTitle("Informe")
                    ->setSubject($this->container->getParameter('nombre_cliente'));

            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Administracion de Caja')
                    ->setCellValue('D1', 'Pesos ($)')
                    ->setCellValue('G1', 'Dolar(u$s)');

            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A2', 'Fecha')
                    ->setCellValue('B2', 'Movimiento')
                    ->setCellValue('C2', 'Debe')
                    ->setCellValue('D2', 'Haber')
                    ->setCellValue('E2', 'Saldo')
                    ->setCellValue('F2', 'Debe')
                    ->setCellValue('G2', 'Haber')
                    ->setCellValue('H2', 'Saldo');

            $count = 3;
            $suma = 0;

            $pesosD = 0;
            $pesosH = 0;
            $dolaresD = 0;
            $dolaresH = 0;
            $tipo = '';

            foreach ($entities as $entity) {
                if ($entity->getTipoDocumento() == 'R' or $entity->getTipoDocumento() == 'RP' or $entity->getTipoDocumento() == 'G' or $entity->getTipoDocumento() == 'A') {

                    if ($entity->getTipoDocumento() == 'R') {
                        $tipo = 'RECIBO CLIENTE' . $entity->getId();
                    }
                    if ($entity->getTipoDocumento() == 'RP') {
                        $tipo = 'RECIBO PROV' . $entity->getId();
                    }
                    if ($entity->getTipoDocumento() == 'G') {
                        $tipo = 'GASTO' . $entity->getId();
                    }
                    if ($entity->getTipoDocumento() == 'A') {
                        $tipo = 'AJUSTE' . $entity->getId();
                    }

                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A' . $count, $entity->getDocumento()->getFechaRegistracion()->format('d-m-Y'))
                            ->setCellValue('B' . $count, $tipo);

                    if ($entity->getTipoDocumento() == 'R') {
                        if ($entity->getMonedaStr() == 'ARG') {
                            $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                            $operacion = $pesosD - $pesosH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('C' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('E' . $count, '$ ' . $operacion);
                        } else {
                            $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                            $operacion = $dolaresD - $dolaresH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('F' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('H' . $count, 'U$S ' . $operacion);
                        }
                    }
                    if ($entity->getTipoDocumento() == 'RP' or $entity->getTipoDocumento() == 'G') {
                        if ($entity->getMonedaStr() == 'ARG') {
                            $pesosH = $entity->getDocumento()->getImporte() + $pesosH;
                            $operacion = $pesosD - $pesosH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('D' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('E' . $count, '$ ' . $operacion);
                        } else {
                            $dolaresH = $entity->getDocumento()->getImporte() + $dolaresH;
                            $operacion = $dolaresD - $dolaresH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('G' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('H' . $count, 'U$S ' . $operacion);
                        }
                    }
                    if ($entity->getTipoDocumento() == 'A') {
                        if ($entity->getMonedaStr() == 'ARG') {
                            if ($entity->getDocumento()->getImporte() < 0) {
                                $pesosH = $entity->getDocumento()->getImporte() * (-1) + $pesosH;
                                $operacion = $pesosD - $pesosH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('D' . $count, '$ ' . $entity->getDocumento()->getImporte() * (-1))
                                        ->setCellValue('E' . $count, '$ ' . $operacion);
                            } else {
                                $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                                $operacion = $pesosD - $pesosH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('C' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                        ->setCellValue('E' . $count, '$ ' . $operacion);
                            }
                        } else {
                            if ($entity->getDocumento()->getImporte() < 0) {
                                $dolaresH = $entity->getDocumento()->getImporte() * (-1) + $dolaresH;
                                $operacion = $dolaresD - $dolaresH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('G' . $count, 'U$S ' . $entity->getDocumento()->getImporte() * (-1))
                                        ->setCellValue('H' . $count, 'U$S ' . $operacion);
                            } else {
                                $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                                $operacion = $dolaresD - $dolaresH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('F' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                        ->setCellValue('H' . $count, 'U$S ' . $operacion);
                            }
                        }
                    }
                    $count++;
                }
            }

            $sumaPesos = $pesosD - $pesosH;
            $sumaDolares = $dolaresD - $dolaresH;

            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('D' . $count, 'SALDO CAJA $ ' . round($sumaPesos, 2))
                    ->setCellValue('G' . $count, 'SALDO CAJA U$S ' . round($sumaDolares, 2));

            $phpExcelObject->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');


            $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $phpExcelObject->setActiveSheetIndex(0);
            $phpExcelObject->getActiveSheet()->getStyle('D' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
            $phpExcelObject->getActiveSheet()->getStyle('G' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
            $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=Administracion Caja.xls');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');

            return $response;
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
    }

    /**
     * Creates a new MovimientoCC$entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new MovimientoCC();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('movimientocc_show', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:MovimientoCC:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MovimientoCC$entity.
     *
     * @param MovimientoCC $entity The$entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MovimientoCC $entity) {
        $form = $this->createForm(new MovimientoCCType(), $entity, array(
            'action' => $this->generateUrl('movimientocc_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MovimientoCC$entity.
     *
     */
    public function newAction() {
        $entity = new MovimientoCC();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:MovimientoCC:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MovimientoCC$entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:MovimientoCC:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing MovimientoCC$entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:MovimientoCC:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a MovimientoCC$entity.
     *
     * @param MovimientoCC $entity The$entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(MovimientoCC $entity) {
        $form = $this->createForm(new MovimientoCCType(), $entity, array(
            'action' => $this->generateUrl('movimientocc_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing MovimientoCC$entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('movimientocc_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:MovimientoCC:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a MovimientoCC$entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MovimientoCC$entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('movimientocc'));
    }

    /**
     * Creates a form to delete a MovimientoCC$entity by id.
     *
     * @param mixed $id The$entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('movimientocc_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function cuentascobrarAction() {
        if ($this->sessionSvc->isLogged()) {
            $em = $this->getDoctrine()->getManager();
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

            return $this->render('JOYASJoyasBundle:MovimientoCC:filtro.html.twig', array(
                        'tipo' => 'cobrar',
                        'unidades' => $unidades
            ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
    }

    public function cuentaspagarAction() {
        if ($this->sessionSvc->isLogged()) {
            $em = $this->getDoctrine()->getManager();
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
            return $this->render('JOYASJoyasBundle:MovimientoCC:filtro.html.twig', array(
                        'tipo' => 'pagar',
                        'unidades' => $unidades
            ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
    }

    public function informeCuentasAction(Request $request) {
        if ($this->sessionSvc->isLogged()) {

            $em = $this->getDoctrine()->getManager();

            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            $phpExcelObject->getProperties()->setCreator($this->container->getParameter('nombre_cliente'))
                    ->setLastModifiedBy($this->container->getParameter('nombre_cliente'))
                    ->setTitle("Informe")
                    ->setSubject($this->container->getParameter('nombre_cliente'));
            if ($request->get('listado') == "1") {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Fecha: ' . date('d-m-Y'))
                        ->setCellValue('C1', 'Moneda: ARG');
            } else {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Fecha' . date('d-m-Y'))
                        ->setCellValue('C1', 'Moneda: USD');
            }

            if ($request->get('tipo') == 'cobrar') {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A2', 'CLIENTE')
                        ->setCellValue('B2', 'FECHA ULTIMA COMPRA')
                        ->setCellValue('C2', 'IMPORTE ULTIMA COMPRA')
                        ->setCellValue('D2', 'FECHA ULTIMO PAGO')
                        ->setCellValue('E2', 'IMPORTE ULTIMO PAGO')
                        ->setCellValue('F2', 'SALDO ACTUAL');

                if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                    $resultados = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getCuentasPorCobrar($request->get('listado'), $this->sessionSvc->getSession('unidad'));
                } else {
                    $resultados = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getCuentasPorCobrar($request->get('listado'), $request->get('unidadnegocio'));
                }

                $count = 3;
                $suma = 0;
                foreach ($resultados as $r) {

                    $pesosD = 0;
                    $pesosH = 0;
                    $clienteProveedor = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($r["id"]);
                    foreach ($clienteProveedor->getMovimientoscc() as $mov) {
                        if ($mov->getEstado() == 'A' and $mov->getMoneda() == $request->get('listado')) {
                            if ($mov->getTipoDocumento() == "R" or $mov->getTipoDocumento() == "C") {
                                $pesosH = $pesosH + $mov->getDocumento()->getImporte();
                                $this->sessionSvc->setSession($mov->getDocumento()->getImporte(), $mov->getDocumento()->getImporte());
                            } else {
                                if ($mov->getTipoDocumento() == "FC") {
                                    $importeFC = 0;
                                    foreach ($mov->getFactura()->getProductosFactura() as $prodFact) {
                                        $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                                    }

                                    if ($mov->getFactura()->getDescuento() != 0 and $mov->getFactura()->getDescuento() != '') {
                                        $desc = $importeFC * ($mov->getFactura()->getDescuento() / 100);
                                        $importeFC = $importeFC - $desc;
                                    }

                                    $importeFC = $importeFC - $mov->getFactura()->getBonificacion();

                                    $pesosD = $pesosD + $importeFC;
                                } else {
                                    if ($mov->getTipoDocumento() == "D") {
                                        $pesosD = $pesosD + $mov->getDocumento()->getImporte();
                                    }
                                }
                            }
                        }
                    }
                    $this->sessionSvc->setSession('debe', $pesosD);
                    $this->sessionSvc->setSession('haber', $pesosH);

                    $saldo = $pesosD - $pesosH;

                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A' . $count, $r["razonSocial"])
                            ->setCellValue('B' . $count, $r["fechaCompra"])
                            ->setCellValue('C' . $count, $r["importeCompra"])
                            ->setCellValue('D' . $count, $r["fechaPago"])
                            ->setCellValue('E' . $count, $r["importePago"])
                            ->setCellValue('F' . $count, $saldo);

                    $suma = $suma + $saldo;
                    $count = $count + 1;
                }

                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('E' . $count, 'TOTAL')
                        ->setCellValue('F' . $count, $suma);

                $phpExcelObject->getActiveSheet()->getStyle('A2:F2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');


                $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);
                $phpExcelObject->getActiveSheet()->getStyle('E' . $count . ':F' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
                $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
                // create the response
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                // adding headers
                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Content-Disposition', 'attachment;filename=InformeCuentasCobrar.xls');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');

                return $response;
            } else {

                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A2', 'PROVEEDOR')
                        ->setCellValue('B2', 'FECHA ULTIMA COMPRA')
                        ->setCellValue('C2', 'IMPORTE ULTIMA COMPRA')
                        ->setCellValue('D2', 'FECHA ULTIMO PAGO')
                        ->setCellValue('E2', 'IMPORTE ULTIMO PAGO')
                        ->setCellValue('F2', 'SALDO ACTUAL');

                if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                    $resultados = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getCuentasPorPagar($request->get('listado'), $this->sessionSvc->getSession('unidad'));
                } else {
                    $resultados = $em->getRepository('JOYASJoyasBundle:MovimientoCC')->getCuentasPorPagar($request->get('listado'), $request->get('unidadnegocio'));
                }

                $count = 3;
                $suma = 0;
                foreach ($resultados as $r) {

                    $pesosD = 0;
                    $pesosH = 0;
                    $clienteProveedor = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($r["id"]);
                    foreach ($clienteProveedor->getMovimientoscc() as $mov) {
                        if ($mov->getEstado() == 'A' and $mov->getMoneda() == $request->get('listado')) {
                            if ($mov->getTipoDocumento() == "F" or $mov->getTipoDocumento() == "D") {
                                $pesosH = $pesosH + $mov->getDocumento()->getImporte();
                            } else {
                                if ($mov->getTipoDocumento() == "RP" or $mov->getTipoDocumento() == "C") {
                                    $pesosD = $pesosD + $mov->getDocumento()->getImporte();
                                }
                            }
                        }
                    }

                    $saldo = $pesosD - $pesosH;

                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A' . $count, $r["razonSocial"])
                            ->setCellValue('B' . $count, $r["fechaCompra"])
                            ->setCellValue('C' . $count, $r["importeCompra"])
                            ->setCellValue('D' . $count, $r["fechaPago"])
                            ->setCellValue('E' . $count, $r["importePago"])
                            ->setCellValue('F' . $count, $saldo);

                    $suma = $suma + $saldo;
                    $count = $count + 1;
                }

                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('E' . $count, 'TOTAL')
                        ->setCellValue('F' . $count, $suma);

                $phpExcelObject->getActiveSheet()->getStyle('A2:F2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');


                $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);
                $phpExcelObject->getActiveSheet()->getStyle('E' . $count . ':F' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
                $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
                // create the response
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                // adding headers
                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Content-Disposition', 'attachment;filename=InformeCuentasPagar.xls');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');

                return $response;
            }
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
    }

}
