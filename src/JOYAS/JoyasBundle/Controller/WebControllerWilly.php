<?php

namespace JOYAS\JoyasBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use JOYAS\JoyasBundle\Entity\ListaPrecio;
use JOYAS\JoyasBundle\Entity\MP;
use JOYAS\JoyasBundle\Entity\Precio;
use JOYAS\JoyasBundle\Entity\Producto;
use JOYAS\JoyasBundle\Entity\DetallePedido;
use JOYAS\JoyasBundle\Entity\Pedido;
use JOYAS\JoyasBundle\Entity\UsuarioWeb;

class WebController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    public function indexAction($page = 1) {
        if (session_status() == 'PHP_SESSION_DISABLED') {
            $this->sessionSvc->startSession();
        }
        $em = $this->getDoctrine()->getManager();

        $lista = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findOneBy(array('descripcion' => 'Lista Web'));
        $imagenesBanner = $em->getRepository('JOYASJoyasBundle:Imagen')->findBy(array('estado'=>'A'));
        $query = $em->getRepository('JOYASJoyasBundle:Producto')->precios($lista->getId());
        $novedades = $em->getRepository('JOYASJoyasBundle:Producto')->novedades();
        
        $cant = 32;
        $novedadesDos = NULL;

        if ($page == 1) {
            $cant = 32 - count($novedades);
            if ($cant <= 0) {
                $cant = 1;
            }
            $novedadesDos = $novedades;
        }

        $paginador = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginador->setMaxPerPage($cant);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Web:index.html.twig', array(
                    'precios' => $paginador,
                    'novedades' => $novedades,
                    'novedadesDos' => $novedadesDos,
                    'imagenesBanner' =>$imagenesBanner
        ));
    }

    public function micuentaAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $idusuario = $request->get('idusuarioweb');

        if (isset($idusuario) and $idusuario != '') {
            $usuarioweb = $em->getRepository('JOYASJoyasBundle:UsuarioWeb')->find($idusuario);
            return $this->render('JOYASJoyasBundle:Web:micuenta.html.twig', array(
                        'usuarioweb' => $usuarioweb,
            ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_iniciar'));
        }
    }

    public function novedadesAction() {
        $em = $this->getDoctrine()->getManager();
        $novedades = $em->getRepository('JOYASJoyasBundle:Producto')->novedades();
        return $this->render('JOYASJoyasBundle:Web:novedades.html.twig', array(
                    'novedades' => $novedades,
        ));
    }

    public function destacadosAction() {
        $em = $this->getDoctrine()->getManager();

        $destacados = $em->getRepository('JOYASJoyasBundle:Producto')->findByDestacado(true);

        return $this->render('JOYASJoyasBundle:Web:destacados.html.twig', array(
                    'destacados' => $destacados,
        ));
    }

    public function ofertasAction() {
        $em = $this->getDoctrine()->getManager();

        $ofertas = $em->getRepository('JOYASJoyasBundle:Producto')->ofertas();

        return $this->render('JOYASJoyasBundle:Web:ofertas.html.twig', array(
                    'ofertas' => $ofertas,
        ));
    }

    public function checkoutAction() {
        $em = $this->getDoctrine()->getManager();
        $precios = new ArrayCollection();
        $lista = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findOneBy(array('descripcion' => 'Lista Web'));
        $politicas = $em->getRepository('JOYASJoyasBundle:Politica')->findBy(array('estado' => 'A'),array('tope'=>'ASC'));
        $carrito = $this->sessionSvc->getSession('carrito');
        $ids = explode(',', $carrito);

        for ($i = 0; $i < count($ids); $i++) {
            if ($ids[$i] != '') {
                $precio = $em->getRepository('JOYASJoyasBundle:Precio')->findOneBy(array('producto' => $ids[$i], 'listaPrecio' => $lista->getId()));
                if (!is_null($precio)) {
                    $precios->add($precio);
                }
            }
        }

        return $this->render('JOYASJoyasBundle:Web:checkout.html.twig', array(
                    'precios' => $precios,
                    'politicas' => $politicas,
        ));
    }

    public function contactoAction() {
        return $this->render('JOYASJoyasBundle:Web:contact.html.twig');
    }

    public function mailAction(Request $request) {
        $mail = 'info@willyjhons.com.ar';
        $mensaje = 'Hola! Le ha llegado el siguiente mensaje desde el sitio web.%0DNombre de contacto:  ' . $request->get('nombre') . '%0DTelefono:  ' . $request->get('telefono') . '%0DEmail:  ' . $request->get('email') . '%0DMensaje:  ' . $request->get('mensaje') . '%0D%0DAnte cualquier duda o consulta, no dude en consultarnos www.sistemastitanio.com.ar';

        file_get_contents('http://sistemastitanio.com.ar/app.php/titanio/mailgenerico?from=info@willyjhons.com.ar&to=' . $mail . '&asunto=Contacto%20Web&mensaje=' . str_replace(' ', '%20', $mensaje));

        $this->sessionSvc->addFlash('msgOk', 'Mensaje enviado correctamente, gracias por su consulta.');
        return $this->redirect($this->generateUrl('joyas_joyas_web'));
    }

    public function irregistroAction(Request $request) {
        return $this->render('JOYASJoyasBundle:Web:register.html.twig');
    }

    public function registroAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('JOYASJoyasBundle:UsuarioWeb')->findOneBy(array('mail' => $request->get('email')));
        if (!is_null($usuario)) {
            $this->sessionSvc->addFlash('msgWarn', 'Ya existe un usuario registrado con ese mail.');
            return $this->redirect($this->generateUrl('joyas_joyas_irregistro'));
        } else {
            $usuario = new UsuarioWeb();
            $usuario->setNombre($request->get('nombre'));
            $usuario->setMail($request->get('email'));
            $usuario->setTelefono($request->get('telefono'));
            $usuario->setClave($request->get('clave'));

            $em->persist($usuario);
            $em->flush();

            $mail = 'info@willyjhons.com';
            $mensaje = 'Se ha registrado un nuevo usuario!%0DNombre de contacto: ' . $request->get('nombre') . '%0DTelefono:  ' . $request->get('telefono') . '%0DEmail: ' . $request->get('email') . '%0DAnte cualquier duda o consulta, no dude en contactarnos www.sistemastitanio.com.ar';

            file_get_contents('http://sistemastitanio.com.ar/app.php/titanio/mailgenerico?from=info@willyjhons.com.ar&to=' . $mail . '&asunto=Registro%20Web&mensaje=' . str_replace(' ', '%20', $mensaje));

            $this->sessionSvc->addFlash('msgOk', 'Gracias por registrarte!.');

            return $this->redirect($this->generateUrl('joyas_joyas_loginusuarioweb', array('mail' => $usuario->getMail(), 'clave' => $usuario->getClave())));
        }

        return $this->redirect($this->generateUrl('joyas_joyas_web'));
    }

    public function cerrarAction() {
        $this->sessionSvc->setSession('imgfb', '');
        $this->sessionSvc->closeSession();
        return $this->redirect($this->generateUrl('joyas_joyas_web'));
    }

    public function realizarpedidoAction(Request $request) {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            $mail = 'jlarrosa@sistemastitanio.com.ar';
        }
        if ($_SERVER['SERVER_NAME'] == 'willyjhons.com.ar' || $_SERVER['SERVER_NAME'] == 'www.willyjhons.com.ar') {
            $mail = 'info@willyjhons.com.ar';
        }        
        if ($this->sessionSvc->getSession('idusuarioweb') == '') {
            $this->sessionSvc->addFlash('msgWarn', 'Para continuar con el pedido loguearse o registrarse, así sabremos como contactarlo.');                        
            return $this->redirect($this->generateUrl('joyas_joyas_iniciar'));                       
        }
        $em = $this->getDoctrine()->getManager();
        //$minimo = $em->getRepository('JOYASJoyasBundle:Politica')->getValorMinimo();

        /*if ($minimo['monto'] > $request->get('resultadofin')) {
            $this->sessionSvc->addFlash('msgWarn', 'El monto minimo debe superar el valor minimo en las "Politicas de Venta" ( $' . $minimo['monto'] . ' )');
            $this->sessionSvc->vaciarcarrito();
            return $this->redirect($this->generateUrl('joyas_joyas_web'));
        }*/

        $usuarioweb = $em->getRepository('JOYASJoyasBundle:UsuarioWeb')->find($this->sessionSvc->getSession('idusuarioweb'));
        $totales = $this->calculartotal($request->get('resultadofin'));

        $pedido = new Pedido();
        $pedido->setObservacion($request->get('comentario'));
        $pedido->setDescuento($totales['desc']);
        $pedido->setTotal($totales['total']);
        $pedido->setEnvio($request->get('enviar'));
        $pedido->setFecha(new \DateTime('NOW'));
        $pedido->setUsuarioweb($usuarioweb);
        $em->persist($pedido);
        $em->flush();

        $carrito = $this->sessionSvc->getSession('carrito');
        $ids = explode(',', $carrito);
        if (count($ids) > 0) {
            for ($i = 0; $i < count($ids); $i++) {
                $cant = 'cantidad' . $ids[$i];
                $precio = 'valor' . $ids[$i];
                $cantidad = $request->get($cant);
                $preciodesc = 'valordescuento' . $ids[$i];
                $preciodescuento = $request->get($preciodesc);

                $valor = $request->get($precio);

                if (isset($cantidad) and $cantidad > 0 and $ids[$i] != '') {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->find($ids[$i]);
                    $detallepedido = new DetallePedido();
                    $detallepedido->setPedido($pedido);
                    $detallepedido->setProducto($producto);

                    if (isset($preciodescuento) && $preciodescuento != '') {
                        $detallepedido->setPrecio($preciodescuento);
                    } else {
                        $detallepedido->setPrecio($valor);
                    }

                    $detallepedido->setCantidad($request->get($cant));
                    $detallepedido->setProductodesc($producto->getDescripcion() . ' ( COD: ' . $producto->getCodigo() . ' )');

                    $em->persist($detallepedido);
                    $em->flush();
                }
            }

            $this->sessionSvc->vaciarcarrito();
            $this->sessionSvc->addFlash('msgOk', 'Pedido realizado, solo falta acordar el medio de pago.');

            if ($pedido->getEnvio() == 'Si') {
                $this->sessionSvc->addFlash('msgOk', 'ENVIO: Seras contactado por el vendedor para acordar el envio por OCA.');
            }

            $message = \Swift_Message::newInstance()
                    ->setSubject('Nuevo Pedido Web')
                    ->setFrom('info@willyjhons.com.ar')
                    ->setTo($mail)
                    ->setBody(
                    $this->renderView('JOYASJoyasBundle:Default:nuevopedido.txt.twig', array('pedido' => $pedido, 'usuarioweb' => $usuarioweb))
            );
            $this->get('mailer')->send($message);
            return $this->redirect($this->generateUrl('joyas_joyas_formapago', array('id' => $pedido->getId())));
        } else {
            $this->sessionSvc->addFlash('msgWarn', 'Algo salio mal.');
            return $this->redirect($this->generateUrl('joyas_joyas_checkout'));
        }
    }

    public function formapagoAction($id) {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);
        $mp = new MP("6714334695549946", "Kkxu0kkNF1JB0sL6ka2JoJp16ATmcO2Z");
        $mp->sandbox_mode(FALSE);
        $items = array();

        foreach ($pedido->getDetallespedido() as $detalle) {
            $titulo = $detalle->getProducto()->getDescripcion() . '(COD: ' . $detalle->getProducto()->getCodigo() . ')';
            $cantidad = $detalle->getCantidad();
            $precio = $detalle->getPrecio();

            $item = array("title" => $titulo, "quantity" => $cantidad, "currency_id" => "ARS", "unit_price" => $precio);

            array_push($items, $item);
        }

        $back = array(
            "success" => 'http://willyjhons.titaniohosting.com.ar/sistemas/titanio/app.php/' . $pedido->getId() . '/success',
            "failure" => 'http://willyjhons.titaniohosting.com.ar/sistemas/titanio/app.php/' . $pedido->getId() . '/failure',
            "pending" => 'http://willyjhons.titaniohosting.com.ar/sistemas/titanio/app.php/' . $pedido->getId() . '/pending');

        $preference_data = array(
            "items" => $items,
            "back_urls" => $back,
            "external_reference" => $pedido->getId());

        $preference = $mp->create_preference($preference_data);
        $this->sessionSvc->setSession('preference', $preference);
        return $this->render('JOYASJoyasBundle:Web:elegirmedio.html.twig', array(
                    'pedido' => $pedido,
//			'pagar' => $preference['response']['sandbox_init_point'],
                    'pagar' => $preference['response']['init_point'],
        ));
    }

    public function acordarAction($id) {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);
        $pedido->setFormapago('O');
        $em->flush();

        $this->sessionSvc->addFlash('msgOk', 'Medio de pago acordado.');
        return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb' => $this->sessionSvc->getSession('idusuarioweb'))));
    }

    public function pedidoAction($id) {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

        return $this->render('JOYASJoyasBundle:Web:pedido.html.twig', array(
                    'pedido' => $pedido,
        ));
    }

    public function successAction($idpedido) {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);
        $pedido->setFormapago('S');
        $em->flush();
        $this->sessionSvc->addFlash('msgOk', 'Pago realizado.');
        return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb' => $this->sessionSvc->getSession('idusuarioweb'))));
    }

    public function failureAction($idpedido) {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);
        $pedido->setFormapago('F');
        $em->flush();
        $this->sessionSvc->addFlash('msgOk', 'El pago ha fallado.');
        return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb' => $this->sessionSvc->getSession('idusuarioweb'))));
    }

    public function pendingAction($idpedido) {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);
        $pedido->setFormapago('P');
        $em->flush();
        $this->sessionSvc->addFlash('msgOk', 'Pago pendiente de aprobacion.');
        return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb' => $this->sessionSvc->getSession('idusuarioweb'))));
    }

    public function estadopedidoAction(Request $request) {
        $idpago = $request->get('idpago');
        $idpedido = $request->get('idpedido');

        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);
        $pedido->setIdpago($idpago);
        $em->flush();

        $mp = new MP("6714334695549946", "Kkxu0kkNF1JB0sL6ka2JoJp16ATmcO2Z");

        $mp->sandbox_mode(FALSE);

        $payment_info = $mp->get_payment_info($idpago);
        $merchant_order_info = $mp->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"]);
        $estado = '';
        foreach ($merchant_order_info["response"]["payments"] as $payment) {
            if ($payment['status'] == 'approved') {
                $pedido->setFormapago('S');
                $estado = 'APROBADO';
            }
            if ($payment['status'] == 'pending') {
                $pedido->setFormapago('P');
                $estado = 'PENDIENTE DE PAGO';
            }
            if ($payment['status'] == 'rejected') {
                $pedido->setFormapago('R');
                $estado = 'RECHAZADO';
            }
            if ($payment['status'] == 'in_process') {
                $pedido->setFormapago('I');
                $estado = 'SIENDO REVISADO';
            }
            if ($payment['status'] == 'cancelled') {
                $pedido->setFormapago('C');
                $estado = 'SIENDO REVISADO';
            }
        }

        $em->flush();
        $this->sessionSvc->addFlash('msgWarn', 'El estado del pago es: ' . $estado);

        return $this->redirect($this->generateUrl('joyas_joyas_web'));
    }

    public function pedidogestionAction($id) {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

        return $this->render('JOYASJoyasBundle:Web:pedidogestion.html.twig', array(
                    'pedido' => $pedido,
        ));
    }

    public function pedidosgestionAction() {
        $em = $this->getDoctrine()->getManager();
        $pedidos = $em->getRepository('JOYASJoyasBundle:Pedido')->findAll();

        return $this->render('JOYASJoyasBundle:Web:pedidos.html.twig', array(
                    'pedidos' => $pedidos,
        ));
    }

    public function calculartotal($totalentrada) {
        $em = $this->getDoctrine()->getManager();
        $totalsalida = $totalentrada;
        $politicas = $em->getRepository('JOYASJoyasBundle:Politica')->getAllActivas();
        $desc = 0;

        foreach ($politicas as $pol) {
            if ($totalsalida >= $pol->getTope()) {
                $desc = $pol->getDescuento();
            } else {
                break;
            }
        }

        if ($desc != 0) {
            $totalsalida = $totalsalida * (1 - ($desc / 100));
        }

        return array('total' => $totalsalida, 'desc' => $desc);
    }

}

?>
