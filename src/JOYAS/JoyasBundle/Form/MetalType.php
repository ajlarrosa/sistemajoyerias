<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MetalType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('descripcion', 'text', array(
                    'label' => 'Metal',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => true
                ))
                ->add('sinonimo', 'text', array(
                    'label' => 'Sinónimo Web',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => false
                ))
                ->add('estado', 'choice', [
                    'choices' => array(
                        'A' => 'Activo',
                        'E' => 'Eliminado'
                    )
                ])

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Metal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'joyas_joyasbundle_metal';
    }
}
