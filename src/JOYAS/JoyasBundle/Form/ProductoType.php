<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ProductoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('categoriasubcategoria', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Categoriasubcategoria',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.estado =:estado')
                                ->setParameter('estado', 'A')
                                ->innerJoin('c.categoria', 'p')
                                ->innerJoin('c.subcategoria', 's')
                                ->addOrderBy('p.descripcion', 'ASC')
                                ->addOrderBy('s.descripcion', 'ASC');
                    }
                ))
                ->add('codigo', 'text', array('label' => 'Codigo', 'attr' => array('class' => 'form-control')))
                ->add('descripcion', 'text', array('label' => 'Nombre', 'attr' => array('class' => 'form-control')))
                ->add('tipoStock', 'choice', array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'U' => 'Unidades',
                        'P' => 'Gramos de Plata',
                        'O' => 'Gramos de Oro'
            )))
                ->add('stock', 'text', 
                        array('label' => 'Stock', 
                            'attr' => array('class' => 'form-control',
                                'pattern' => '[0-9]+([\.][0-9]+)?',
                                  'title' => 'Se espera un número de la forma 000000.00'
                                )
                            
                            ))
                ->add('moneda', 'choice', array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        1 => 'ARG',
                        2 => 'USD'
            )))
                ->add('costo', 'text', array('label' => 'Hechura', 'attr' => array('class' => 'form-control')))
                ->add('plata', 'text', array('label' => 'Plata', 'attr' => array('class' => 'form-control')))
                ->add('oro', 'text', array('label' => 'Oro', 'attr' => array('class' => 'form-control')))
                ->add('quilate', 'text', array('label' => 'Quilate (gr)', 'attr' => array('class' => 'form-control') ,
                    'required' => false))
                ->add('descripcionampliada', 'textarea', array('label' => 'Descripcion',
                    'attr' => array('class' => 'form-control', 'style' => 'height:200px')
                    ,
                    'required' => false
                        )
                )
                ->add('visible', 'checkbox', array('attr' => array('class' => 'form-control')))
                ->add('novedad', 'checkbox', array('attr' => array('class' => 'form-control')))
                ->add('destacado', 'checkbox', array('attr' => array('class' => 'form-control')))
                ->add('descuento', 'integer', array('label' => 'Descuento',
                    'attr' => array('class' => 'form-control', 'max' => '100')
                    ,
                    'required' => false
                        )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Producto'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_producto';
    }

}
