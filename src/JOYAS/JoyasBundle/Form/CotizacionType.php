<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CotizacionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    private $idUnidadNegocio;

    public function __construct($idUnidadNegocio = null) {
        $this->idUnidadNegocio = $idUnidadNegocio;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dolar', 'number', array('label' => 'Dólar',
                    'attr' => array(
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'),
                    'required' => true))
                ->add('oro', 'number', array('label' => 'Oro',
                    'attr' => array('step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'),
                    'required' => true))
                ->add('plata', 'number', array('label' => 'Plata',
                    'attr' => array('step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'),
                    'required' => true))
                ->add('quilate', 'number', array('label' => 'Quilate (u$s/gr)',
                    'attr' => array(
                        'class' => 'form-control',
                        'step' => 0.01,
                        'min' => 0),
                    'required' => true));

        if (isset($this->idUnidadNegocio)) {
            $builder->add('unidadNegocio', 'entity', array(
                'attr' => array(
                    'class' => 'form-control'),
                'class' => 'JOYASJoyasBundle:UnidadNegocio',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                                    ->where('c.estado = :estado')
                                    ->andWhere('c.id = :unidadNegocio')
                                    ->setParameter(':estado', 'A')
                                    ->setParameter(':unidadNegocio', $this->idUnidadNegocio)
                                    ->addOrderBy('c.descripcion', 'ASC');
                }
            ));
        } else {
            $builder->add('unidadNegocio', 'entity', array(
                 'required'    => false,
                'attr' => array(
                    'class' => 'form-control'),
                'class' => 'JOYASJoyasBundle:UnidadNegocio',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                                    ->where('c.estado = :estado')
                                    ->setParameter(':estado', 'A')
                                    ->addOrderBy('c.descripcion', 'ASC');
                }
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Cotizacion'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_cotizacion';
    }

}
