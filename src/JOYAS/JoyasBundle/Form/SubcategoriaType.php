<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubcategoriaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('sinonimo', 'text', array(
                    'label' => 'Sinónimo Web',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => false
                ))
                ->add('descripcion', 'text', array(
                    'label' => 'SubCategoría',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => true
                ))
                 ->add('estado', 'choice', [
                    'choices' => array(
                        'A' => 'Activo',
                        'E' => 'Eliminado'
                    )
                ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Subcategoria'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_subcategoria';
    }

}
